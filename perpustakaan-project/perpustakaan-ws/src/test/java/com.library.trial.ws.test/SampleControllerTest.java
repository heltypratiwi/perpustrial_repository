package com.library.trial.ws.test;


import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


/**
 * Created by kris on 9/1/2014.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-test.xml"})
@WebAppConfiguration
public class SampleControllerTest {

  /*  private MockMvc mockMvc;
    @InjectMocks
    private SampleController sampleController;
    @Mock
    private AgamaService mockService;
    @Autowired
    private MockHttpSession mockHttpSession;
    @Autowired
    private AgamaService agamaService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = standaloneSetup(sampleController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        mockHttpSession.setAttribute(Constant.SESSION_USER, new User(1l));
    }

    @Test
    public void getAll() throws Exception {
        Mockito.when(mockService.find(new Agama(), 0, 10)).thenReturn(agamaService.find(new Agama(), 0, 10));

        MvcResult result = this.mockMvc.perform(get("/agamas/").accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andReturn();
    }

    @Test
    @Transactional
    public void saveObject() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        Agama agama = new Agama();
        agama.setNama("KONGUAN");

        Mockito.when(mockService.save(agama)).thenReturn(agamaService.save(agama));

        mockMvc.perform(
                post("/agamas/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(agama)))
                .andExpect(status().isOk()).andDo(print());
    }

    @Test
    @Transactional
    public void updateObject() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        Agama agama = new Agama();
        agama.setId(28l);
        agama.setNama("KONGUAN");

        Mockito.when(mockService.save(agama)).thenReturn(agamaService.save(agama));

        mockMvc.perform(
                put("/agamas/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(agama)))
                .andExpect(status().isOk()).andDo(print());
    }

    @Test
    //@Transactional
    public void deleteObject() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        Agama agama = new Agama();
        agama.setId(28l);
        agama.setNama("KONGUAN");

        Mockito.when(mockService.save(agama)).thenReturn(agamaService.save(agama));

        mockMvc.perform(
                delete("/agamas/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(agama)))
                .andExpect(status().isOk()).andDo(print());
    }*/
}
