package com.library.trial.ws.controller;

import org.springframework.stereotype.Controller;

/**
 * Created by kris on 9/1/2014.
 */
@Controller
public class SampleController extends BaseController {

  /*  @Autowired
    private AgamaService agamaService;

    @RequestMapping(value = "/agamas", method = RequestMethod.GET)
    public ResponseEntity<List<Agama>> find() {
        return new ResponseEntity<>(agamaService.find(new Agama(), 0, 10), HttpStatus.OK);
    }

    @RequestMapping(value = "/agamas/{id}", method = RequestMethod.GET)
    public ResponseEntity<Agama> find(@PathVariable("id") long id) {
        Agama agama = agamaService.findById(id);
        if (agama == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(agama, HttpStatus.OK);
    }

    @RequestMapping(value = "/agamas/", method = RequestMethod.POST)
    public ResponseEntity<Result> save(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
        Result result = agamaService.save(agama);
        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/agamas/", method = RequestMethod.PUT)
    public ResponseEntity<Result> update(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
        Result result = agamaService.save(agama);
        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/agamas/", method = RequestMethod.DELETE)
    public ResponseEntity<Result> delete(@RequestBody Agama agama, HttpServletRequest request, HttpServletResponse response) {
        Result result = agamaService.save(agama);
        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
*/
}
