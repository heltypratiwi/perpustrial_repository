package com.library.trial.web.server.service;

import com.library.trial.core.Profile;
import com.library.trial.web.client.service.GwtHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtHomeService")
public class HomeServiceImpl implements GwtHomeService {
    @Autowired(required = false)
    private Profile profile;

    @Override
    public String getAppTitle() {
        return profile.getAppTittle();
    }
}
