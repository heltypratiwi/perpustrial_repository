package com.library.trial.web.client.view.menu.master;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Menu;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.GridView;
import com.library.trial.web.client.view.custom.ToolbarGridView;
import com.library.trial.web.client.view.custom.ToolbarSearchType;
import com.library.trial.web.client.view.custom.WindowSearchView;
import com.library.trial.web.client.view.handler.GridHandler;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/15/2017.
 */
public class MahasiswaView extends View implements GridHandler{

    private Grid<Mahasiswa> grid;
    private PagingToolBar pagingToolBar;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Mahasiswa>> pagingLoader;
    private WindowSearchView windowSearchView;
    private Mahasiswa param;

    public MahasiswaView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
        ListStore<Mahasiswa> listStore = new ListStore<>(getProperties().getMahasiswaProperties().key());

        ColumnConfig<Mahasiswa, String> colNim = new ColumnConfig<>(getProperties().getMahasiswaProperties().valueNIM(), 70, "NIM");
        ColumnConfig<Mahasiswa, String> colNama = new ColumnConfig<>(getProperties().getMahasiswaProperties().valueNama(), 120, "NAMA MAHASISWA");
        ColumnConfig<Mahasiswa, String> colJurusan = new ColumnConfig<>(getProperties().getMahasiswaProperties().valueJurusan(), 100, "JURUSAN");
        ColumnConfig<Mahasiswa, String> colAlamat = new ColumnConfig<>(getProperties().getMahasiswaProperties().valueAlamat(), 70, "ALAMAT");

        ArrayList<ColumnConfig<Mahasiswa, ?>> list = new ArrayList<>();
        list.add(colNim);
        list.add(colNama);
        list.add(colJurusan);
        list.add(colAlamat);
        ColumnModel<Mahasiswa> columnModel = new ColumnModel<>(list);

        RpcProxy<PagingLoadConfig, PagingLoadResult<Mahasiswa>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Mahasiswa>>() {
            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Mahasiswa>> callback) {
                param = new Mahasiswa();
                param.setNIM(windowSearchView.getTextFieldNIM().getCurrentValue());
                param.setNamaMahasiswa(windowSearchView.getTextFieldNama().getCurrentValue());
                param.setJurusan(windowSearchView.getTextFieldJurusan().getCurrentValue());
                param.setAlamat(windowSearchView.getTextFieldAlamat().getCurrentValue());

                getService().getMahasiswaServiceAsync().find(param, loadConfig, callback);
            }
        };

        pagingLoader = new PagingLoader<>(proxy);
        pagingLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Mahasiswa, PagingLoadResult<Mahasiswa>>(listStore));

        pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(pagingLoader);

        grid = new Grid<Mahasiswa>(listStore, columnModel) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        pagingLoader.load();
                    }
                });
            }
        };

        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);
        grid.setLoader(pagingLoader);
        grid.setLoadMask(true);
        grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        windowSearchView = new WindowSearchView(ToolbarSearchType.MAHASISWA) {
            @Override
            public SelectEvent.SelectHandler buttonSearchHandler() {
                return new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        pagingToolBar.setActivePage(1);
                        pagingToolBar.refresh();
                        windowSearchView.hide();
                    }
                };
            }
        };

        GridView gridView = new GridView(getMenu());
        gridView.addWidget(createToolbar(), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        return gridView;
    }

    private Widget createToolbar() {
        return new ToolbarGridView(super.getMenu()) {
            @Override
            public SelectEvent.SelectHandler buttonToolbarAddSelectHandler() {
                return buttonAddSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarEditSelectHandler() {
                return buttonEditSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler() {
                return buttonDeleteSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarSearchSelectHandler() {
                return buttonSearchSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarPrintSelectHandler() {
                return buttonPrintSelectHandler();
            }
        }.asWidget();
    }

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new MahasiswaFormView().showForm(MahasiswaView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new MahasiswaFormView().showForm(MahasiswaView.this);
                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final Mahasiswa mahasiswa = grid.getSelectionModel().getSelectedItem();
                if (mahasiswa == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                final ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
                    @Override
                    public void onDialogHide(DialogHideEvent event) {
                        if (event.getHideButton().toString().equals(AppClient.MESSAGE_YES)) {
                            getService().getMahasiswaServiceAsync().delete(mahasiswa, new AsyncCallback<Result>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    AppClient.showMessageOnFailureException(caught);
                                }

                                @Override
                                public void onSuccess(Result result) {
                                    AppClient.showInfoMessage(result.getMessage());
                                    if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                        getPagingToolBar().refresh();
                                    }
                                }
                            });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                windowSearchView.show();
            }

        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

            }
        };
    }

    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                new MahasiswaFormView().showForm(MahasiswaView.this);
            }
        };
    }


    public PagingToolBar getPagingToolBar() {
        return pagingToolBar;
    }

    public Grid<Mahasiswa> getGrid() {
        return grid;
    }

    public WindowSearchView getWindowSearchView() {
        return windowSearchView;
    }

}



