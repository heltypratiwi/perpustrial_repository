package com.library.trial.web.client.view.menu.Transaksi;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.DateType;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.CustomDateField;
import com.library.trial.web.client.view.custom.CustomFieldLabel;
import com.library.trial.web.client.view.custom.WindowFormView;
import com.library.trial.web.client.view.handler.FormHandler;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Created by yoggi on 5/10/2017.
 */
public class PeminjamanFormView extends View implements FormHandler{
    private WindowFormView windowFormView ;
    private PeminjamanView parentView;
    private TextField textFieldIdBuku;
    private TextField textFieldJudul;
    private TextField textFieldMahasiswa;
    private TextField textFieldIdMahasiswa;
    private CustomDateField tanggalPinjam;
    private CustomDateField tanggalKembali;

    private Peminjaman peminjaman;
    @Override
    public void showForm(View view) {
        parentView = (PeminjamanView) view;
        textFieldIdBuku = new TextField();
        textFieldIdBuku.setAllowBlank(false);
        textFieldJudul = new TextField();
        textFieldJudul.setAllowBlank(false);

        textFieldMahasiswa = new TextField();
        textFieldMahasiswa.setAllowBlank(false);
        textFieldIdMahasiswa = new TextField();
        textFieldIdMahasiswa.setAllowBlank(false);
        peminjaman = new Peminjaman();
        tanggalPinjam = new CustomDateField(DateType.DEFAULT);
        tanggalKembali = new CustomDateField(DateType.DEFAULT);


        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            peminjaman = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldIdBuku.setValue(String.valueOf(peminjaman.getBuku().getId()));
            textFieldJudul.setValue(String.valueOf(peminjaman.getBuku().getJudul()));
            textFieldIdMahasiswa.setValue(String.valueOf(peminjaman.getMahasiswa().getId()));
            textFieldMahasiswa.setValue(String.valueOf(peminjaman.getMahasiswa().getNIM()));
            tanggalPinjam.setValue(peminjaman.getCreatedTime());
            tanggalKembali.setValue(peminjaman.getCreatedTime());

        } else {
            peminjaman = new Peminjaman();
        }
/*
        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(new CustomFieldLabel(textFieldKode, "Kode Buku"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldJudul, "Judul "), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldNim, "NIM"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldNama, "Nama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        textFieldJudul.setEnabled(false);
        textFieldNama.setEnabled(false);
        textFieldKode.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                Buku buku = new Buku();
                buku.setKode(textFieldKode.getText());
                getService().getBukuServiceAsync().find(buku, new AsyncCallback<ArrayList<Buku>>() {
                    @Override
                    public void onFailure(Throwable throwable) {

                    }
*/

        VerticalLayoutContainer verticalLayoutContainerLeft = new VerticalLayoutContainer();
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldIdBuku, "ID BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldJudul, "JUDUL BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(tanggalPinjam, "TANGGAL PINJAM "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));

        VerticalLayoutContainer verticalLayoutContainerRight = new VerticalLayoutContainer();
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldIdMahasiswa, "ID MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldMahasiswa, "NIM MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerRight.add(new CustomFieldLabel(tanggalKembali, "TANGGAL KEMBALI "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));


        HBoxLayoutContainer hBoxLayoutContainer = new HBoxLayoutContainer();
        hBoxLayoutContainer.add(verticalLayoutContainerLeft, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 5, 0, 0)));
        hBoxLayoutContainer.add(verticalLayoutContainerRight, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 5, 0, 0)));

        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(hBoxLayoutContainer, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldIdBuku);

        windowFormView.show();
    }
/*
                    @Override
                    public void onSuccess(ArrayList<Buku> bukus) {
                        textFieldJudul.setText(bukus.get(0).getJudul());
                    }
                });

            }
        });


        textFieldNim.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                final Mahasiswa mahasiswa = new Mahasiswa();
                mahasiswa.setNIM(textFieldNim.getText());
                getService().getMahasiswaServiceAsync().find(mahasiswa, new AsyncCallback<ArrayList<Mahasiswa>>() {
                    @Override
                    public void onFailure(Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(ArrayList<Mahasiswa> mahasiswas) {
                        textFieldNama.setText(mahasiswas.get(0).getNama());
                    }
                });
            }
        });
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldKode);

        windowFormView.show();
    }
*/
    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }
        Buku buku = new Buku();
        buku.setId(Long.valueOf(textFieldIdBuku.getCurrentValue()));
        buku.setJudul(textFieldJudul.getCurrentValue());

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(Long.valueOf(textFieldIdMahasiswa.getCurrentValue()));
        mahasiswa.setNIM(textFieldMahasiswa.getCurrentValue());

        peminjaman.setBuku(buku);
        peminjaman.setMahasiswa(mahasiswa);
        peminjaman.setTanggalPinjam(tanggalPinjam.getCurrentValue());
       // peminjaman.setKodeBuku(textFieldKode.getCurrentValue());
         //       peminjaman.setJudulBuku(textFieldJudul.getCurrentValue());
         //       peminjaman.setNimMahasiswa(textFieldNim.getCurrentValue());
         //       peminjaman.setNamaMahasiswa(textFieldNama.getCurrentValue());

                getService().getPeminjamanServiceAsync().save(peminjaman, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);

                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }
                        AppClient.showInfoMessage(result.getMessage(), parentView.getPagingToolBar());
                    }
                });

            }
        };

    }
}
