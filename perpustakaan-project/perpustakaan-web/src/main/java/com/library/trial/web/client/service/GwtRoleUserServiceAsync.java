/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.entity.RoleUser;
import com.library.trial.core.entity.User;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public interface GwtRoleUserServiceAsync {

    void find(User user, AsyncCallback<ArrayList<RoleUser>> callback);
}
