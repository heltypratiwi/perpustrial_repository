package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.entity.Menu;
import com.library.trial.core.entity.Role;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtMenuService")
public interface GwtMenuService {
    ArrayList<Menu> createTreeMenu();

    ArrayList<Menu> createTreeMenu(Role role);
}
