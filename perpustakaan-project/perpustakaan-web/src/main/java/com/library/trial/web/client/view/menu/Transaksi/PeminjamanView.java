package com.library.trial.web.client.view.menu.Transaksi;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.common.Pattern;
import com.library.trial.core.common.Result;
import com.library.trial.core.common.DateType;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Menu;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.*;
import com.library.trial.web.client.view.handler.GridHandler;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
public class PeminjamanView extends View implements GridHandler {
    private Grid<Peminjaman> grid;
    private PagingToolBar pagingToolBar;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Peminjaman>> pagingLoader;
    private WindowSearchView windowSearchView;
    private Peminjaman param;
    private TextField textFieldId;
    private TextField textFieldKode;
    private TextField textFieldJudul;
    private TextField textFieldISBN;
    private TextField textFieldNomerRak;
    private TextField textFieldNIM;
    private TextField textFieldIdMahasiswa;
    private CustomDateField tanggalPinjam;
    private TextField textFieldNamaMahasiswa;
    private TextField textFieldJurusan;

    private Peminjaman peminjaman;
    private TextButtonSave textButtonSave;
    private TextButtonDelete textButtonDelete;
    private Mahasiswa mahasiswa;
    private Buku buku;

    public PeminjamanView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {

        textFieldId = new TextField();
        textFieldId.setAllowBlank(false);
        textFieldId.hide();
        textFieldKode = new TextField();
        textFieldKode.setAllowBlank(false);
        textFieldKode.addKeyDownHandler(keyBukuDownHandler());
        textFieldJudul = new TextField();
        textFieldJudul.setAllowBlank(false);

        textFieldISBN = new TextField();
        textFieldISBN.setAllowBlank(false);
        textFieldNomerRak = new TextField();
        textFieldNomerRak.setAllowBlank(false);

        textFieldNIM = new TextField();
        textFieldNIM.setAllowBlank(false);
        textFieldId = new TextField();
        textFieldId.setAllowBlank(false);
        textFieldId.hide();
        textFieldNIM.addKeyDownHandler(keyMahasiswaDownHandler());
        textFieldNamaMahasiswa = new TextField();
        textFieldNamaMahasiswa.setAllowBlank(false);
        textFieldJurusan = new TextField();
        textFieldJurusan.setAllowBlank(false);

        tanggalPinjam = new CustomDateField(DateType.DEFAULT);
        textButtonSave = new TextButtonSave();
        textButtonSave.addSelectHandler(textButtonSaveHandler());
        textButtonDelete = new TextButtonDelete();
        textButtonDelete.addSelectHandler(buttonDeleteSelectHandler());
        peminjaman = new Peminjaman();

        buku = new Buku();

        ListStore<Peminjaman> listStore = new ListStore<>(getProperties().getPeminjamanProperties().key());

        ColumnConfig<Peminjaman, Date> colTanggalPinjam = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueTanggalPinjam(), 120, "TANGGAL PINJAM");
        ColumnConfig<Peminjaman, String> colKode= new ColumnConfig<>(getProperties().getPeminjamanProperties().valueKode(), 100, "KODE BUKU");
        ColumnConfig<Peminjaman, String> colJudul = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueJudul(), 100, "BUKU YANG DIPINJAM");
        ColumnConfig<Peminjaman, String> colNIM = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueNim(), 100, "NIM ");
        ColumnConfig<Peminjaman, String> colNama = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueNama(), 100, "NAMA");

        ColumnConfig<Peminjaman, String> colCreatedBy = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueCreatedBy(), 80, "CREATED BY");
        ColumnConfig<Peminjaman, Date> colCreatedTime = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueCreatedTime(), 80, "CREATED TIME");
        colCreatedTime.setCell(new DateCell(DateTimeFormat.getFormat(Pattern.DATE_TIME_PATTERN)));

        ArrayList<ColumnConfig<Peminjaman, ?>> list = new ArrayList<>();
        list.add(colTanggalPinjam);
        list.add(colKode);
        list.add(colJudul);
        list.add(colNIM);
        list.add(colNama);
        list.add(colCreatedBy);
        list.add(colCreatedTime);
        ColumnModel<Peminjaman> columnModel = new ColumnModel<>(list);

        RpcProxy<PagingLoadConfig, PagingLoadResult<Peminjaman>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Peminjaman>>() {
            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Peminjaman>> callback) {
                param = new Peminjaman();
                param.setTanggalPinjam(windowSearchView.getTanggalKembali().getValue());
                getService().getPeminjamanServiceAsync().find(param, loadConfig, callback);
            }
        };

        pagingLoader = new PagingLoader<>(proxy);
        pagingLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Peminjaman, PagingLoadResult<Peminjaman>>(listStore));


        pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(pagingLoader);

        grid = new Grid<Peminjaman>(listStore, columnModel) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        pagingLoader.load();
                    }
                });
            }
        };

        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);
        grid.setLoader(pagingLoader);
        grid.setLoadMask(true);
        grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        windowSearchView = new WindowSearchView(ToolbarSearchType.PEMINJAMAN) {
            @Override
            public SelectEvent.SelectHandler buttonSearchHandler() {
                return new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        pagingToolBar.setActivePage(1);
                        pagingToolBar.refresh();
                        windowSearchView.hide();
                    }
                };
            }
        };

        VerticalLayoutContainer verticalLayoutContainerRight = new VerticalLayoutContainer();
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldKode, "KODE BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldJudul, "JUDUL BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldISBN, "ISBN "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldNomerRak, "NOMER RAK "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));


        VerticalLayoutContainer verticalLayoutContainerLeft = new VerticalLayoutContainer();
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNIM, "NIM MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNamaMahasiswa, "NAMA MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldJurusan, "JURUSAN "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(tanggalPinjam, "TANGGAL PINJAM "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));

        BoxLayoutContainer.BoxLayoutData flex = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0));
        flex.setFlex(1);

        HBoxLayoutContainer hBoxLayoutContainer = new HBoxLayoutContainer();
        hBoxLayoutContainer.add(verticalLayoutContainerLeft, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 15, 5, 10)));
        hBoxLayoutContainer.add(verticalLayoutContainerRight, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 15, 5, 10)));
        hBoxLayoutContainer.add(new FieldLabel(), flex);
        hBoxLayoutContainer.add(new FieldLabel(), flex);

        HBoxLayoutContainer hBoxLayoutContainerButton = new HBoxLayoutContainer();
        hBoxLayoutContainerButton.setPadding(new Padding(5));
        hBoxLayoutContainerButton.add(new FieldLabel(), flex);

        hBoxLayoutContainerButton.add(textButtonSave, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
        hBoxLayoutContainerButton.add(textButtonDelete, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));

        GridView gridView = new GridView(getMenu());
        gridView.setHeaderVisible(false);
        gridView.setBodyBorder(false);
        gridView.setBorders(false);
        gridView.addWidget(hBoxLayoutContainer, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(hBoxLayoutContainerButton, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        /*FramedPanel framedPanel = new FramedPanel();
        framedPanel.add(hBoxLayoutContainer);
        framedPanel.add(hBoxLayoutContainerButton);*/
        return gridView;
/*
        GridView gridView = new GridView(getMenu());
        gridView.addWidget(createToolbar(), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        return gridView;
        */
    }

    private KeyDownHandler keyBukuDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {

                if (keyDownEvent.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {

                    Buku param = new Buku();
                    param.setKode(textFieldKode.getCurrentValue());

                    getService().getBukuServiceAsync().findByKode(param, new AsyncCallback<Buku>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(Buku result) {
                            buku = result;
                            if (result != null) {
                                textFieldJudul.setValue(result.getJudul());
                                textFieldISBN.setValue(result.getIsbn());
                                textFieldNomerRak.setValue(result.getNomerRak());
                                System.out.println(result);

                            } else {
                                MessageBox messageBox = AppClient.showInfoMessage(Result.DATA_NOT_EXIST);
                                messageBox.addHideHandler(new HideEvent.HideHandler() {
                                    @Override
                                    public void onHide(HideEvent event) {
                                        textFieldKode.focus();
                                    }
                                });
                                textFieldJudul.reset();
                                textFieldISBN.reset();
                                textFieldNomerRak.reset();
                            }
                        }
                    });
                }
            }
        };
    }

    private KeyDownHandler keyMahasiswaDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {
                if (keyDownEvent.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {

                    Mahasiswa param = new Mahasiswa();
                    param.setNIM(textFieldNIM.getCurrentValue());

                    getService().getMahasiswaServiceAsync().findByNim(param, new AsyncCallback<Mahasiswa>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(Mahasiswa result) {
                            mahasiswa = result;
                            if (result != null) {
                                textFieldNamaMahasiswa.setValue(result.getNamaMahasiswa());
                                textFieldJurusan.setValue(result.getJurusan());

                                peminjaman = new Peminjaman();
                                peminjaman.setMahasiswa(mahasiswa);
                                getService().getPeminjamanServiceAsync().find(peminjaman, new AsyncCallback<ArrayList<Peminjaman>>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<Peminjaman> peminjamans) {
                                        grid.getStore().replaceAll(peminjamans);
                                    }
                                });

                            } else {
                                MessageBox messageBox = AppClient.showInfoMessage(Result.DATA_NOT_EXIST);
                                messageBox.addHideHandler(new HideEvent.HideHandler() {
                                    @Override
                                    public void onHide(HideEvent event) {
                                        textFieldNIM.focus();
                                    }
                                });
                                textFieldNamaMahasiswa.reset();
                                textFieldJurusan.reset();
                                textFieldIdMahasiswa.reset();
                            }
                        }
                    });
                }
            }
        };
    }


/*
    private Widget createToolbar() {
        return new ToolbarGridView(super.getMenu()) {
            @Override
            public SelectEvent.SelectHandler buttonToolbarAddSelectHandler() {
                return buttonAddSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarEditSelectHandler() {
                return buttonEditSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler() {
                return buttonDeleteSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarSearchSelectHandler() {
                return buttonSearchSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarPrintSelectHandler() {
                return buttonPrintSelectHandler();
            }

        }.asWidget();

    }
    */

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new PeminjamanFormView().showForm(PeminjamanView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new PeminjamanFormView().showForm(PeminjamanView.this);
                    Buku buku = new Buku();
                    buku.setId(Long.valueOf(textFieldKode.getCurrentValue()));
                    buku.setJudul(textFieldJudul.getCurrentValue());

                    Mahasiswa mahasiswa = new Mahasiswa();
                    mahasiswa.setId(Long.valueOf(textFieldIdMahasiswa.getCurrentValue()));
                    mahasiswa.setNIM(textFieldNIM.getCurrentValue());

                    peminjaman.setBuku(buku);
                    peminjaman.setMahasiswa(mahasiswa);
                    peminjaman.setTanggalPinjam(tanggalPinjam.getCurrentValue());
                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final Peminjaman peminjaman = grid.getSelectionModel().getSelectedItem();
                if (peminjaman == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                final ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
                    @Override
                    public void onDialogHide(DialogHideEvent event) {
                        if (event.getHideButton().toString().equals(AppClient.MESSAGE_YES)) {
                            getService().getPeminjamanServiceAsync().delete(peminjaman, new AsyncCallback<Result>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    AppClient.showMessageOnFailureException(caught);
                                }

                                @Override
                                public void onSuccess(Result result) {
                                    AppClient.showInfoMessage(result.getMessage());
                                    if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                        getPagingToolBar().refresh();
                                    }
                                }
                            });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                windowSearchView.show();
            }

        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

            }
        };
    }

    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
               // new PeminjamanFormView().showForm(PeminjamanView.this);
                peminjaman = grid.getSelectionModel().getSelectedItem();
                System.out.println(peminjaman);
                textFieldKode.setValue(peminjaman.getBuku().getKode().toString());
                textFieldJudul.setValue(peminjaman.getBuku().getJudul());
                textFieldISBN.setValue(peminjaman.getBuku().getIsbn());
                textFieldNomerRak.setValue(peminjaman.getBuku().getNomerRak());


                textFieldNIM.setValue(peminjaman.getMahasiswa().getNIM());
                tanggalPinjam.setValue(peminjaman.getTanggalPinjam());
            }
        };
    }

    private SelectEvent.SelectHandler textButtonSaveHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

                buku.setJudul(textFieldJudul.getCurrentValue());
                mahasiswa.setNIM(textFieldNIM.getCurrentValue());

                peminjaman.setBuku(buku);
                peminjaman.setMahasiswa(mahasiswa);
                peminjaman.setTanggalPinjam(tanggalPinjam.getCurrentValue());

                getService().getPeminjamanServiceAsync().save(peminjaman, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        AppClient.showInfoMessage(result.getMessage());
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            getPagingToolBar().refresh();
                            clear();
                        }
                    }
                });
            }
        };
    }

    private void clear() {
        textFieldKode.setText("");
        textFieldJudul.setText("");
        textFieldISBN.setText("");
        textFieldNomerRak.setText("");

        textFieldNIM.setText("");
        textFieldId.setText("");
        textFieldNamaMahasiswa.setText("");
        textFieldJurusan.setText("");
        tanggalPinjam = new CustomDateField(DateType.DEFAULT);
    }

    public PagingToolBar getPagingToolBar() {
        return pagingToolBar;
    }

    public Grid<Peminjaman> getGrid() {
        return grid;
    }

    public WindowSearchView getWindowSearchView() {
        return windowSearchView;
    }

}
