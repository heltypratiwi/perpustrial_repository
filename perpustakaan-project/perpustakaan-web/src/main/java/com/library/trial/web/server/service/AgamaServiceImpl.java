package com.library.trial.web.server.service;

import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Agama;
import com.library.trial.core.service.AgamaService;
import com.library.trial.web.client.service.GwtAgamaService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtAgamaService")
public class AgamaServiceImpl implements GwtAgamaService {
    @Autowired
    private AgamaService agamaService;

    @Override
    public Result save(Agama agama) {
        return agamaService.save(agama);
    }

    @Override
    public Result delete(Agama agama) {
        return agamaService.delete(agama);
    }

    @Override
    public PagingLoadResult<Agama> find(Agama agama, PagingLoadConfig pagingLoadConfig) {
        return new PagingLoadResultBean<>(agamaService.find(agama, pagingLoadConfig.getOffset(),
                pagingLoadConfig.getLimit()),
                agamaService.count(agama), pagingLoadConfig.getOffset()
        );
    }

    @Override
    public ArrayList<Agama> find(Agama agama) {
        return new ArrayList<>(agamaService.find(agama, 0, Integer.MAX_VALUE));
}}
