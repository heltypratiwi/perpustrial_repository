package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Peminjaman;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtPeminjamanServiceAsync {

    void save(Peminjaman peminjaman, AsyncCallback<Result> callback);

    void delete(Peminjaman peminjaman, AsyncCallback<Result> callback);

    void find(Peminjaman peminjaman, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Peminjaman>> callback);

    void find(Peminjaman peminjaman, AsyncCallback<ArrayList<Peminjaman>> callback);

}
