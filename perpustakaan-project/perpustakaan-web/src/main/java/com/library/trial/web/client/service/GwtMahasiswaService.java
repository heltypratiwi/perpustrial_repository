package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Mahasiswa;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtMahasiswaService")
public interface GwtMahasiswaService extends RemoteService{
    Result save(Mahasiswa mahasiswa);

    Result delete(Mahasiswa mahasiswa);

    PagingLoadResult<Mahasiswa> find(Mahasiswa mahasiswa, PagingLoadConfig pagingLoadConfig);

    ArrayList<Mahasiswa> find(Mahasiswa mahasiswa);

    Mahasiswa findByNim(Mahasiswa mahasiswa);

}
