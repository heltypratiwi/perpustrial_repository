package com.library.trial.web.client.view.menu.Transaksi;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.common.DateType;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Menu;
import com.library.trial.core.entity.Pengembalian;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.*;
import com.library.trial.web.client.view.handler.GridHandler;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yoggi on 5/15/2017.
 */
public class PengembalianView extends View implements GridHandler {
    private Grid<Pengembalian> grid;
    private PagingToolBar pagingToolBar;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Pengembalian>> pagingLoader;
    private WindowSearchView windowSearchView;
    private Pengembalian param;
    private TextField textFieldIdBuku;
    private TextField textFieldJudul;
    private TextField textFieldNIM;
    private CustomDateField tanggalPinjam;
    private TextField textFieldNamaMahasiswa;
    private TextField textFieldJurusan;
    private TextField textFieldIdMahasiswa;
    private CustomDateField tanggalKembali;
    private Pengembalian pengembalian;
    private TextButtonSave textButtonSave;
    private TextButtonDelete textButtonDelete;
    private TextButtonSearch textButtonSearch;
    private Mahasiswa mahasiswa;
    private Buku buku;

    public PengembalianView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
        textFieldIdBuku = new TextField();
        textFieldIdBuku.setAllowBlank(false);
        textFieldJudul = new TextField();
        textFieldJudul.setAllowBlank(false);
        textFieldIdBuku = new TextField();
        textFieldIdBuku.setAllowBlank(false);
        textFieldNIM = new TextField();
        textFieldNIM.setAllowBlank(false);
        textFieldNIM.addKeyDownHandler(keyMahasiswaDownHandler());
        textFieldIdMahasiswa = new TextField();
        textFieldIdMahasiswa.setAllowBlank(false);
        textFieldNamaMahasiswa = new TextField();
        textFieldNamaMahasiswa.setAllowBlank(false);
        textFieldJurusan = new TextField();
        textFieldJurusan.setAllowBlank(false);
        tanggalKembali = new CustomDateField(DateType.DEFAULT);
        textButtonSave = new TextButtonSave();
        textButtonSave.addSelectHandler(textButtonSaveHandler());
        textButtonDelete = new TextButtonDelete();
        textButtonDelete.addSelectHandler(buttonDeleteSelectHandler());
        pengembalian = new Pengembalian();

        ListStore<Pengembalian> listStore = new ListStore<>(getProperties().getPengembalianProperties().key());

        ColumnConfig<Pengembalian, String> colIdBuku = new ColumnConfig<>(getProperties().getPengembalianProperties().valueIdBuku(), 120, "BUKU YANG DIPINJAM");
        ColumnConfig<Pengembalian, Date> colTanggalPinjam = new ColumnConfig<>(getProperties().getPengembalianProperties().valueTanggalPinjam(), 120, "TANGGAL PINJAM");
        ColumnConfig<Pengembalian, Date> colTanggalKembali = new ColumnConfig<>(getProperties().getPengembalianProperties().valueTanggalKembali(), 120, "TANGGAL PENGEMBALIAN");
        ColumnConfig<Pengembalian, String> colCreateBy = new ColumnConfig<>(getProperties().getPengembalianProperties().valueCreatedBy(), 80, "CREATED BY");

        ArrayList<ColumnConfig<Pengembalian, ?>> list = new ArrayList<>();
        list.add(colIdBuku);
        list.add(colTanggalPinjam);
        list.add(colTanggalKembali);
        list.add(colCreateBy);
        ColumnModel<Pengembalian> columnModel = new ColumnModel<>(list);

        RpcProxy<PagingLoadConfig, PagingLoadResult<Pengembalian>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Pengembalian>>() {
            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Pengembalian>> callback) {
                param = new Pengembalian();
                param.setTanggalKembali(windowSearchView.getTanggalKembali().getCurrentValue());
                getService().getPengembalianServiceAsync().find(param, loadConfig, callback);
            }
        };

        pagingLoader = new PagingLoader<>(proxy);
        pagingLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Pengembalian, PagingLoadResult<Pengembalian>>(listStore));

        pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(pagingLoader);

        grid = new Grid<Pengembalian>(listStore, columnModel) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        pagingLoader.load();
                    }
                });
            }
        };

        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);
        grid.setLoader(pagingLoader);
        grid.setLoadMask(true);
        grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        windowSearchView = new WindowSearchView(ToolbarSearchType.PEMINJAMAN) {
            @Override
            public SelectEvent.SelectHandler buttonSearchHandler() {
                return new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        pagingToolBar.setActivePage(1);
                        pagingToolBar.refresh();
                        windowSearchView.hide();
                    }
                };
            }
        };

        VerticalLayoutContainer verticalLayoutContainerLeft = new VerticalLayoutContainer();
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNIM, "NIM MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNamaMahasiswa, "NAMA MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldJurusan, "JURUSAN "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(tanggalKembali, "TANGGAL KEMBALI "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));

        VerticalLayoutContainer verticalLayoutContainerRight = new VerticalLayoutContainer();
        BoxLayoutContainer.BoxLayoutData flex = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0));
        flex.setFlex(1);

        HBoxLayoutContainer hBoxLayoutContainer = new HBoxLayoutContainer();
        hBoxLayoutContainer.add(verticalLayoutContainerLeft, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 15, 5, 10)));
        hBoxLayoutContainer.add(new FieldLabel(), flex);
        hBoxLayoutContainer.add(verticalLayoutContainerRight, new BoxLayoutContainer.BoxLayoutData(new Margins(10, 15, 5, 10)));
        hBoxLayoutContainer.add(new FieldLabel(), flex);
        hBoxLayoutContainer.add(new FieldLabel(), flex);

        HBoxLayoutContainer hBoxLayoutContainerButton = new HBoxLayoutContainer();
        hBoxLayoutContainerButton.setPadding(new Padding(5));
        hBoxLayoutContainerButton.add(new FieldLabel(), flex);

        hBoxLayoutContainerButton.add(textButtonSave, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
        hBoxLayoutContainerButton.add(textButtonDelete, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));

        GridView gridView = new GridView(getMenu());
        gridView.setHeaderVisible(false);
        gridView.setBodyBorder(false);
        gridView.setBorders(false);
        gridView.addWidget(hBoxLayoutContainer, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(hBoxLayoutContainerButton, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        return gridView;
    }

    private KeyDownHandler keyMahasiswaDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {
                if (keyDownEvent.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {

                    Mahasiswa param = new Mahasiswa();
                    System.out.println("OK " + textFieldNIM.getCurrentValue());
                    param.setNIM(textFieldNIM.getCurrentValue());

                    getService().getMahasiswaServiceAsync().findByNim(param, new AsyncCallback<Mahasiswa>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(Mahasiswa result) {
                            mahasiswa = result;
                            if (result != null) {
                                textFieldNamaMahasiswa.setValue(result.getNamaMahasiswa());
                                textFieldJurusan.setValue(result.getJurusan());

                                pengembalian = new Pengembalian();
                                pengembalian.setMahasiswa(mahasiswa);
                                getService().getPengembalianServiceAsync().find(pengembalian, new AsyncCallback<ArrayList<Pengembalian>>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {

                                    }
                                    @Override
                                    public void onSuccess(ArrayList<Pengembalian> pengembalians) {
                                        grid.getStore().replaceAll(pengembalians);
                                        System.out.println(pengembalians);
                                    }
                                });
                            } else {
                                MessageBox messageBox = AppClient.showInfoMessage(Result.DATA_NOT_EXIST);
                                messageBox.addHideHandler(new HideEvent.HideHandler() {
                                    @Override
                                    public void onHide(HideEvent event) {
                                        textFieldNIM.focus();
                                    }
                                });
                                textFieldNamaMahasiswa.setText("");
                                textFieldJurusan.setText("");
                            }

                        }
                    });

                }
            }
        };}

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new PeminjamanFormView().showForm(PengembalianView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new PeminjamanFormView().showForm(PengembalianView.this);
                    Buku buku = new Buku();
                    buku.setId(Long.valueOf(textFieldIdBuku.getCurrentValue()));
                    buku.setJudul(textFieldJudul.getCurrentValue());

                    Mahasiswa mahasiswa = new Mahasiswa();
                    mahasiswa.setId(Long.valueOf(textFieldIdMahasiswa.getCurrentValue()));
                    mahasiswa.setNIM(textFieldNIM.getCurrentValue());

                    pengembalian.setBuku(buku);
                    pengembalian.setMahasiswa(mahasiswa);
                    pengembalian.setTanggalKembali(tanggalKembali.getCurrentValue());

                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final Pengembalian pengembalian = grid.getSelectionModel().getSelectedItem();
                if (pengembalian == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                final ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
                    @Override
                    public void onDialogHide(DialogHideEvent event) {
                        if (event.getHideButton().toString().equals(AppClient.MESSAGE_YES)) {
                            getService().getPengembalianServiceAsync()
                                    .delete(pengembalian, new AsyncCallback<Result>() {
                                        @Override
                                        public void onFailure(Throwable caught) {
                                            AppClient.showMessageOnFailureException(caught);
                                        }

                                        @Override
                                        public void onSuccess(Result result) {
                                            AppClient.showInfoMessage(result.getMessage());
                                            if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                                getPagingToolBar().refresh();
                                            }
                                        }
                                    });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                windowSearchView.show();
            }

        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

            }
        };
    }

    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                pengembalian = grid.getSelectionModel().getSelectedItem();
                System.out.println(pengembalian);
                textFieldNIM.setValue(pengembalian.getMahasiswa().getNIM());
                textFieldNamaMahasiswa.setValue(pengembalian.getMahasiswa().getNamaMahasiswa());
                textFieldJurusan.setValue(pengembalian.getMahasiswa().getJurusan());
                tanggalKembali.setValue(pengembalian.getTanggalKembali());
            }
        };
    }

    private SelectEvent.SelectHandler textButtonSaveHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                Buku buku = grid.getSelectionModel().getSelectedItem().getBuku();
                System.out.println(buku);

                Mahasiswa mahasiswa = grid.getSelectionModel().getSelectedItem().getMahasiswa();
                System.out.println(mahasiswa);

                pengembalian.setBuku(buku);
                pengembalian.setMahasiswa(mahasiswa);
                pengembalian.setTanggalKembali(tanggalKembali.getCurrentValue());

                getService().getPengembalianServiceAsync().save(pengembalian, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        AppClient.showInfoMessage(result.getMessage());
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            getPagingToolBar().refresh();
                            clear();
                        }
                    }
                });
            }
        };
    }

    private void clear() {
        textFieldNIM.setText("");
        textFieldJurusan.setText("");
        textFieldNamaMahasiswa.setText("");
    }

    public PagingToolBar getPagingToolBar() {
        return pagingToolBar;
    }

    public Grid<Pengembalian> getGrid() {
        return grid;
    }

    public WindowSearchView getWindowSearchView() {
        return windowSearchView;
    }
}





