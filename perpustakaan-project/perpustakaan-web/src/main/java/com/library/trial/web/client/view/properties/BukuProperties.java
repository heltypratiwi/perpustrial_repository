package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Buku;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface BukuProperties extends PropertyAccess<Buku> {
    @Editor.Path("Id")
    ModelKeyProvider<Buku> key();

    @Editor.Path("Kode")
    LabelProvider<Buku> labelKode();

    @Editor.Path("Kode")
    ValueProvider<Buku, String> valueKode();

    @Editor.Path("Judul")
    LabelProvider<Buku> labelJudul();

    @Editor.Path("Judul")
    ValueProvider<Buku, String> valueJudul();

    @Editor.Path("Isbn")
    LabelProvider<Buku> labelIsbn();

    @Editor.Path("Isbn")
    ValueProvider<Buku, String> valueIsbn();

    @Editor.Path("NomerRak")
    LabelProvider<Buku> labelNomerRak();

    @Editor.Path("NomerRak")
    ValueProvider<Buku, String> valueNomerRak();

    @Editor.Path("createdTime")
    ValueProvider<Buku, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Buku, String> valueCreatedBy();
}
