package com.library.trial.web.server.controller;

import com.library.trial.core.common.Result;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yoggi on 5/10/2017.
 */
public class BaseController {
    public Map convertModel(Object data, Result result) {
        Map model = new HashMap();
        model.put("data", data);
        model.put("status", result);
        return model;
    }

    /**
     *
     * @param data
     * @param status
     * @return
     */
    public Map<String, Object> convertModel(Object data, Object status) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("status", status);
        return model;
    }
}
