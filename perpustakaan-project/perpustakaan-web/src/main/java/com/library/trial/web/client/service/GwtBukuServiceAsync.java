package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtBukuServiceAsync {
    void save(Buku buku, AsyncCallback<Result> asyncCallback);

    void delete(Buku buku, AsyncCallback<Result> callback);

    void findByKode(Buku buku, AsyncCallback<Buku> callback);

    void find(Buku buku, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Buku>> callback);

    void find(Buku buku, AsyncCallback<ArrayList<Buku>> callback);
}
