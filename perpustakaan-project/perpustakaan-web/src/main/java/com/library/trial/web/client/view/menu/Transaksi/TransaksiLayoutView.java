package com.library.trial.web.client.view.menu.Transaksi;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.common.DateType;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.entity.Pengembalian;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.*;
import com.sencha.gxt.core.client.resources.ThemeStyles;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yoggi on 5/16/2017.
 */
public class TransaksiLayoutView extends View implements IsWidget, EntryPoint {
    private TextField textFieldKode;
    private TextField textFieldJudul;
    private TextField textFieldISBN;
    private TextField textFieldNomerRak;
    private TextField textFieldNIM;
    private TextField textFieldIdMahasiswa;
    private CustomDateField tanggalPinjam;
    private TextField textFieldNamaMahasiswa;
    private TextField textFieldJurusan;
    private CustomDateField tanggalKembali;
    private Pengembalian pengembalian;
    private Peminjaman peminjaman;
    private TextButtonSave textButtonSave;
    private TextButtonDelete textButtonDelete;
    private Mahasiswa mahasiswa;
    private Buku buku;
    private Grid<Peminjaman> grid;
    private Grid<Pengembalian> gridPengembalian;
    private PagingToolBar pagingToolBar;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Peminjaman>> pagingLoader;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Pengembalian>> pagingLoaderPengembalian;
    private WindowSearchView windowSearchView;
    private Peminjaman param;


    public TransaksiLayoutView (Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {

        HorizontalLayoutContainer con = new HorizontalLayoutContainer();
        con.add(peminjamanLayout(), new HorizontalLayoutContainer.HorizontalLayoutData(.50, 1, new Margins(4)));
        con.add(pengembalianLayout(), new HorizontalLayoutContainer.HorizontalLayoutData(.50, 1, new Margins(4)));

        return con;
    }

    public Widget peminjamanLayout() {

    textFieldKode = new TextField();
    textFieldKode.setAllowBlank(false);
    textFieldKode.addKeyDownHandler(keyBukuDownHandler());
    textFieldJudul = new TextField();
    textFieldJudul.setAllowBlank(false);
    textFieldISBN = new TextField();
    textFieldISBN.setAllowBlank(false);
    textFieldNomerRak = new TextField();
    textFieldNomerRak.setAllowBlank(false);

    textFieldNIM = new TextField();
    textFieldNIM.setAllowBlank(false);
    textFieldNIM.addKeyDownHandler(keyMahasiswaDownHandler());
    textFieldNamaMahasiswa = new TextField();
    textFieldNamaMahasiswa.setAllowBlank(false);
    textFieldJurusan = new TextField();
    textFieldJurusan.setAllowBlank(false);

    tanggalPinjam = new CustomDateField(DateType.DEFAULT);
    textButtonSave = new TextButtonSave();
    textButtonDelete = new TextButtonDelete();


    ListStore<Peminjaman> listStore = new ListStore<>(getProperties().getPeminjamanProperties().key());

    ColumnConfig<Peminjaman, Date> colTanggalPinjam = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueTanggalPinjam(), 200, "TANGGAL PINJAM");
    ColumnConfig<Peminjaman, String> colNIM = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueNim(), 150, "NIM");
    ColumnConfig<Peminjaman, String> colJudul = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueKode(), 250, "BUKU YANG DIPINJAM");
    ColumnConfig<Peminjaman, String> colCreateBy = new ColumnConfig<>(getProperties().getPeminjamanProperties().valueCreatedBy(), 80, "CREATED BY");

    ArrayList<ColumnConfig<Peminjaman, ?>> list = new ArrayList<>();
    list.add(colTanggalPinjam);
    list.add(colNIM);
    list.add(colJudul);
    list.add(colCreateBy);

    ColumnModel<Peminjaman> columnModel = new ColumnModel<>(list);

    RpcProxy<PagingLoadConfig, PagingLoadResult<Peminjaman>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Peminjaman>>() {
        @Override
        public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Peminjaman>> callback) {
            Peminjaman param = new Peminjaman();
            param.setTanggalPinjam(windowSearchView.getTanggalKembali().getValue());
            getService().getPeminjamanServiceAsync().find(param, loadConfig, callback);
        }
    };

    pagingLoader = new PagingLoader<>(proxy);
    pagingLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Peminjaman, PagingLoadResult<Peminjaman>>(listStore));

    pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
    pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
    pagingToolBar.bind(pagingLoader);

    grid = new Grid<Peminjaman>(listStore, columnModel) {
        @Override
        protected void onAfterFirstAttach() {
            super.onAfterFirstAttach();
            Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                @Override
                public void execute() {
                    pagingLoader.load();
                }
            });
        }
    };

    grid.getView().setStripeRows(true);
    grid.getView().setColumnLines(true);
    grid.getView().setAutoFill(false);
    grid.setLoader(pagingLoader);
    grid.setLoadMask(true);
    grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

    windowSearchView = new WindowSearchView(ToolbarSearchType.PEMINJAMAN) {
        @Override
        public SelectEvent.SelectHandler buttonSearchHandler() {
            return new SelectEvent.SelectHandler() {
                @Override
                public void onSelect(SelectEvent event) {
                    pagingToolBar.setActivePage(1);
                    pagingToolBar.refresh();
                    windowSearchView.hide();
                }
            };
        }
    };

        VerticalLayoutContainer verticalLayoutContainerPeminjaman = new VerticalLayoutContainer();
        HorizontalLayoutContainer hlc = new HorizontalLayoutContainer();
        hlc.setStyleName(ThemeStyles.get().style().border());
        verticalLayoutContainerPeminjaman.add(hlc, new VerticalLayoutContainer.VerticalLayoutData(1, 1, new Margins(10, 15, 5, 10)));

    VerticalLayoutContainer verticalLayoutContainerRight = new VerticalLayoutContainer();
    verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldKode, "KOE BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
    verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldJudul, "JUDUL BUKU "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
    verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldISBN, "ISBN "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
    verticalLayoutContainerRight.add(new CustomFieldLabel(textFieldNomerRak, "NOMER RAK "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));

    BoxLayoutContainer.BoxLayoutData flex = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0));
    flex.setFlex(1);
    HBoxLayoutContainer hBoxLayoutContainerButton = new HBoxLayoutContainer();
    hBoxLayoutContainerButton.setPadding(new Padding(5));
    hBoxLayoutContainerButton.add(new FieldLabel(), flex);
    hBoxLayoutContainerButton.add(textButtonSave, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
    hBoxLayoutContainerButton.add(textButtonDelete, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
    verticalLayoutContainerRight.add(hBoxLayoutContainerButton, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
    verticalLayoutContainerRight.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
    verticalLayoutContainerRight.add(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
    hlc.add(verticalLayoutContainerRight, new HorizontalLayoutContainer.HorizontalLayoutData(1, 1, new Margins(4)));

    return hlc;

}

    public Widget pengembalianLayout() {
        textFieldNIM = new TextField();
        textFieldNIM.setAllowBlank(false);
        tanggalKembali = new CustomDateField(DateType.DEFAULT);
        textFieldNIM.addKeyDownHandler(keyMahasiswaDownHandler());
        textFieldNamaMahasiswa = new TextField();
        textFieldNamaMahasiswa.setAllowBlank(false);
        textFieldJurusan = new TextField();
        textFieldJurusan.setAllowBlank(false);
        tanggalPinjam = new CustomDateField(DateType.DEFAULT);
        textButtonSave = new TextButtonSave();
        textButtonDelete = new TextButtonDelete();

        ListStore<Pengembalian> listStorePengembalian = new ListStore<>(getProperties().getPengembalianProperties().key());

        ColumnConfig<Pengembalian, String> colKode = new ColumnConfig<>(getProperties().getPengembalianProperties().valueIdBuku(), 200, "BUKU YANG DIPINJAM");
        ColumnConfig<Pengembalian, Date> colTanggalPinjamPengembalian = new ColumnConfig<>(getProperties().getPengembalianProperties().valueTanggalPinjam(), 150, "TANGGAL PINJAM");
        ColumnConfig<Pengembalian, Date> colTanggalKembali = new ColumnConfig<>(getProperties().getPengembalianProperties().valueTanggalKembali(), 150, "TANGGAL PENGEMBALIAN");

        ArrayList<ColumnConfig<Pengembalian, ?>> listPengembalian = new ArrayList<>();
        listPengembalian.add(colKode);
        listPengembalian.add(colTanggalPinjamPengembalian);
        listPengembalian.add(colTanggalKembali);

        ColumnModel<Pengembalian> columnModelPengembalian = new ColumnModel<>(listPengembalian);

        RpcProxy<PagingLoadConfig, PagingLoadResult<Pengembalian>> proxyPengembalian = new RpcProxy<PagingLoadConfig, PagingLoadResult<Pengembalian>>() {
            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Pengembalian>> callback) {
                Pengembalian param = new Pengembalian();
                param.setTanggalKembali(windowSearchView.getTanggalKembali().getCurrentValue());
                getService().getPengembalianServiceAsync().find(param, loadConfig, callback);
            }
        };

        pagingLoaderPengembalian = new PagingLoader<>(proxyPengembalian);
        pagingLoaderPengembalian.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Pengembalian, PagingLoadResult<Pengembalian>>(listStorePengembalian));

        pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(pagingLoaderPengembalian);

        gridPengembalian = new Grid<Pengembalian>(listStorePengembalian, columnModelPengembalian) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        pagingLoaderPengembalian.load();
                    }
                });
            }
        };

        gridPengembalian.getView().setStripeRows(true);
        gridPengembalian.getView().setColumnLines(true);
        gridPengembalian.getView().setAutoFill(false);
        gridPengembalian.setLoader(pagingLoaderPengembalian);
        gridPengembalian.setLoadMask(true);
        gridPengembalian.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        windowSearchView = new WindowSearchView(ToolbarSearchType.PEMINJAMAN) {
            @Override
            public SelectEvent.SelectHandler buttonSearchHandler() {
                return new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        pagingToolBar.setActivePage(1);
                        pagingToolBar.refresh();
                        windowSearchView.hide();
                    }
                };
            }
        };

        VerticalLayoutContainer verticalLayoutContainerPengembalian = new VerticalLayoutContainer();
        HorizontalLayoutContainer horizontalLayoutContainer = new HorizontalLayoutContainer();
        horizontalLayoutContainer.setStyleName(ThemeStyles.get().style().border());
        verticalLayoutContainerPengembalian.add(horizontalLayoutContainer, new VerticalLayoutContainer.VerticalLayoutData(1, 1, new Margins(10, 15, 5, 10)));
        VerticalLayoutContainer verticalLayoutContainerLeft = new VerticalLayoutContainer();
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNIM, "NIM MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1, new Margins(5, 0, 0, 0)));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldNamaMahasiswa, "NAMA MAHASISWA "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(textFieldJurusan, "JURUSAN "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));
        verticalLayoutContainerLeft.add(new CustomFieldLabel(tanggalKembali, "TANGGAL KEMBALI "), new VerticalLayoutContainer.VerticalLayoutData(-1, -1));

        BoxLayoutContainer.BoxLayoutData flex1 = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0));
        flex1.setFlex(1);
        HBoxLayoutContainer hBoxLayoutContainerButton1 = new HBoxLayoutContainer();
        hBoxLayoutContainerButton1.setPadding(new Padding(5));
        hBoxLayoutContainerButton1.add(new FieldLabel(), flex1);
        hBoxLayoutContainerButton1.add(textButtonSave, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
        hBoxLayoutContainerButton1.add(textButtonDelete, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 5, 0, 0)));
        verticalLayoutContainerLeft.add(hBoxLayoutContainerButton1, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        verticalLayoutContainerLeft.add(gridPengembalian, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        verticalLayoutContainerLeft.add(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        horizontalLayoutContainer.add(verticalLayoutContainerLeft, new HorizontalLayoutContainer.HorizontalLayoutData(1, 1, new Margins(4)));

        return horizontalLayoutContainer;
    }

    private RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandlerPengembalian() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                pengembalian = gridPengembalian.getSelectionModel().getSelectedItem();
                System.out.println(pengembalian);
                textFieldNIM.setValue(pengembalian.getMahasiswa().getNIM());
                textFieldNamaMahasiswa.setValue(pengembalian.getMahasiswa().getNamaMahasiswa());
                textFieldJurusan.setValue(pengembalian.getMahasiswa().getJurusan());
                tanggalKembali.setValue(pengembalian.getTanggalKembali());
            }
        };
    }

    @Override
    public void onModuleLoad() {
        RootPanel.get().add(asWidget());
    }

    private KeyDownHandler keyBukuDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {

                if (keyDownEvent.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {

                    Buku param = new Buku();
                    param.setKode(textFieldKode.getCurrentValue());

                    getService().getBukuServiceAsync().findByKode(param, new AsyncCallback<Buku>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }
                        @Override
                        public void onSuccess(Buku result) {
                            buku = result;
                            if (result != null) {
                                textFieldJudul.setValue(result.getJudul());
                                textFieldISBN.setValue(result.getIsbn());
                                textFieldNomerRak.setValue(result.getNomerRak());
                                System.out.println(result);

                            } else {
                                MessageBox messageBox = AppClient.showInfoMessage(Result.DATA_NOT_EXIST);
                                messageBox.addHideHandler(new HideEvent.HideHandler() {
                                    @Override
                                    public void onHide(HideEvent event) {
                                        textFieldKode.focus();
                                    }
                                });
                                textFieldJudul.reset();
                                textFieldNomerRak.reset();

                            }
                        }
                    });
                }
            }
        };
    }
    private KeyDownHandler keyMahasiswaDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent keyDownEvent) {
                if (keyDownEvent.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {

                    Mahasiswa param = new Mahasiswa();
                    param.setNIM(textFieldNIM.getCurrentValue());

                    getService().getMahasiswaServiceAsync().findByNim(param, new AsyncCallback<Mahasiswa>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(Mahasiswa result) {
                            mahasiswa = result;
                            if (result != null) {
                                textFieldNamaMahasiswa.setValue(result.getNamaMahasiswa());
                                textFieldJurusan.setValue(result.getJurusan());

                                peminjaman = new Peminjaman();
                                peminjaman.setMahasiswa(mahasiswa);
                                getService().getPeminjamanServiceAsync().find(peminjaman, new AsyncCallback<ArrayList<Peminjaman>>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<Peminjaman> peminjamans) {
                                        grid.getStore().replaceAll(peminjamans);
                                    }
                                });

                            } else {
                                MessageBox messageBox = AppClient.showInfoMessage(Result.DATA_NOT_EXIST);
                                messageBox.addHideHandler(new HideEvent.HideHandler() {
                                    @Override
                                    public void onHide(HideEvent event) {
                                        textFieldNIM.focus();
                                    }
                                });
                                textFieldNamaMahasiswa.reset();
                                textFieldJurusan.reset();

                            }
                        }
                    });
                }
            }
        };
    }
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final Peminjaman peminjaman = grid.getSelectionModel().getSelectedItem();
                if (peminjaman == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                final ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
                    @Override
                    public void onDialogHide(DialogHideEvent event) {
                        if (event.getHideButton().toString().equals(AppClient.MESSAGE_YES)) {
                            getService().getPeminjamanServiceAsync().delete(peminjaman, new AsyncCallback<Result>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    AppClient.showMessageOnFailureException(caught);
                                }

                                @Override
                                public void onSuccess(Result result) {
                                    AppClient.showInfoMessage(result.getMessage());
                                    if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                        getPagingToolBar().refresh();
                                    }
                                }
                            });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    private SelectEvent.SelectHandler textButtonSaveHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

                buku.setJudul(textFieldJudul.getCurrentValue());
                mahasiswa.setNIM(textFieldNIM.getCurrentValue());

                peminjaman.setBuku(buku);
                peminjaman.setMahasiswa(mahasiswa);
                peminjaman.setTanggalPinjam(tanggalPinjam.getCurrentValue());

                getService().getPeminjamanServiceAsync().save(peminjaman, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        AppClient.showInfoMessage(result.getMessage());
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            getPagingToolBar().refresh();
                            clear();
                        }
                    }
                });
            }
        };
    }

    private SelectEvent.SelectHandler textButtonSavePengembalianHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                Buku buku = gridPengembalian.getSelectionModel().getSelectedItem().getBuku();
                System.out.println(buku);

                Mahasiswa mahasiswa = gridPengembalian.getSelectionModel().getSelectedItem().getMahasiswa();
                System.out.println(mahasiswa);

                pengembalian.setBuku(buku);
                pengembalian.setMahasiswa(mahasiswa);
                pengembalian.setTanggalKembali(tanggalKembali.getCurrentValue());

                getService().getPengembalianServiceAsync().save(pengembalian, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        AppClient.showInfoMessage(result.getMessage());
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            getPagingToolBar().refresh();
                            clear();
                        }
                    }
                });
            }
        };
    }

    private void clear() {
        textFieldKode.reset();
        textFieldJudul.reset();
        textFieldISBN.reset();
        textFieldNomerRak.reset();

        textFieldNIM.reset();
        textFieldNamaMahasiswa.reset();
        textFieldJurusan.reset();
        tanggalPinjam = new CustomDateField(DateType.DEFAULT);
    }


    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                peminjaman = grid.getSelectionModel().getSelectedItem();
                System.out.println(peminjaman);
                textFieldKode.setValue(peminjaman.getBuku().getKode().toString());
                textFieldJudul.setValue(peminjaman.getBuku().getJudul());
                textFieldISBN.setValue(peminjaman.getBuku().getIsbn());
                textFieldNomerRak.setValue(peminjaman.getBuku().getNomerRak());

                textFieldNIM.setValue(peminjaman.getMahasiswa().getNIM());
                tanggalPinjam.setValue(peminjaman.getTanggalPinjam());
            }
        };
    }

    public PagingToolBar getPagingToolBar() {
        return pagingToolBar;
    }
}


