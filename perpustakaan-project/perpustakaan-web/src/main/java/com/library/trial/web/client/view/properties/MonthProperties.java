package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.common.Month;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface MonthProperties extends PropertyAccess<Month>{
    @Editor.Path("month")
    ModelKeyProvider<Month> key();

    @Editor.Path("month")
    ValueProvider<Month, Integer> valueMonth();

    @Editor.Path("monthName")
    ValueProvider<Month, String> valueMonthName();

    @Editor.Path("monthName")
    LabelProvider<Month> labelMonthName();
}

