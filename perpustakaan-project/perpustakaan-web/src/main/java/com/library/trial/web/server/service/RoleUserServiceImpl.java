package com.library.trial.web.server.service;

import com.library.trial.core.entity.RoleUser;
import com.library.trial.core.entity.User;
import com.library.trial.core.service.RoleUserService;
import com.library.trial.web.client.service.GwtRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtRoleUserService")
public class RoleUserServiceImpl implements GwtRoleUserService {

    @Autowired
    private RoleUserService service;

    public ArrayList<RoleUser> find(User user) {
        return new ArrayList<>(service.find(user));
    }
}
