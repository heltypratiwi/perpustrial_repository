package com.library.trial.web.server.service;

import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.service.BukuService;
import com.library.trial.web.client.service.GwtBukuService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtBukuService")
public class BukuServiceImpl implements GwtBukuService {
    @Autowired
    private BukuService bukuService;

    @Override
    public Result save(Buku buku) {
        return bukuService.save(buku);
    }

    @Override
    public Result delete(Buku buku) {
        return bukuService.delete(buku);
    }

    @Override
    public PagingLoadResult<Buku> find(Buku buku, PagingLoadConfig pagingLoadConfig) {
        return new PagingLoadResultBean<>(bukuService.find(buku, pagingLoadConfig.getOffset(),
                pagingLoadConfig.getLimit()),
                bukuService.count(buku), pagingLoadConfig.getOffset()
        );
    }

    @Override
    public ArrayList<Buku> find(Buku buku) {
        return new ArrayList<>(bukuService.find(buku, 0, Integer.MAX_VALUE));
    }

    @Override
    public Buku findByKode(Buku buku) {
        return bukuService.findByKode(buku);
    }
}
