package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Peminjaman;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface PeminjamanProperties extends PropertyAccess<Peminjaman>{
    @Editor.Path("id")
    ModelKeyProvider<Peminjaman> key();

    @Editor.Path("kodeBuku")
    LabelProvider<Peminjaman> labelKode();

    @Editor.Path("kodeBuku")
    ValueProvider<Peminjaman, String> valueKode();

    @Editor.Path("judulBuku")
    LabelProvider<Peminjaman> labelJudul();

    @Editor.Path("judulBuku")
    ValueProvider<Peminjaman, String> valueJudul();

    @Editor.Path("nimMahasiswa")
    LabelProvider<Peminjaman> labelNim();

    @Editor.Path("nimMahasiswa")
    ValueProvider<Peminjaman, String> valueNim();

    @Editor.Path("namaMahasiswa")
    LabelProvider<Peminjaman> labelNama();

    @Editor.Path("namaMahasiswa")
    ValueProvider<Peminjaman, String> valueNama();

    @Editor.Path("tanggalPinjam")
    LabelProvider<Peminjaman> labelTanggalPinjam();

    @Editor.Path("tanggalPinjam")
    ValueProvider<Peminjaman, Date> valueTanggalPinjam();

    @Editor.Path("createdTime")
    ValueProvider<Peminjaman, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Peminjaman, String> valueCreatedBy();
}
