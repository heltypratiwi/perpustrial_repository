package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Mahasiswa;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtMahasiswaServiceAsync {

    void save(Mahasiswa mahasiswa, AsyncCallback<Result> async);

    void delete(Mahasiswa mahasiswa, AsyncCallback<Result> async);

    void findByNim(Mahasiswa mahasiswa, AsyncCallback<Mahasiswa> callback);

    void find(Mahasiswa mahasiswa, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Mahasiswa>> async);

    void find(Mahasiswa mahasiswa, AsyncCallback<ArrayList<Mahasiswa>> async);
}
