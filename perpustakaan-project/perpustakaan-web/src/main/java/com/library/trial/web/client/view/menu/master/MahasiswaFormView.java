package com.library.trial.web.client.view.menu.master;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.CustomFieldLabel;
import com.library.trial.web.client.view.custom.WindowFormView;
import com.library.trial.web.client.view.handler.FormHandler;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Created by yoggi on 5/15/2017.
 */
public class MahasiswaFormView extends View implements FormHandler{

    private WindowFormView windowFormView;
    private MahasiswaView parentView;
    private TextField textFieldNim;
    private TextField textFieldNama;
    private TextField textFieldJurusan;
    private TextField textFieldAlamat;

    private Mahasiswa mahasiswa;

    @Override
    public void showForm(View view) {
        parentView = (MahasiswaView) view;
        textFieldNim = new TextField();
        textFieldNim.setAllowBlank(false);
        textFieldNama = new TextField();
        textFieldNama.setAllowBlank(false);
        textFieldAlamat = new TextField();
        textFieldAlamat.setAllowBlank(false);
        textFieldJurusan = new TextField();
        textFieldJurusan.setAllowBlank(false);


        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            mahasiswa = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldNim.setValue(mahasiswa.getNIM());
            textFieldNama.setValue(mahasiswa.getNamaMahasiswa());
            textFieldAlamat.setValue(mahasiswa.getAlamat());
            textFieldJurusan.setValue(mahasiswa.getJurusan());

        } else {
            mahasiswa = new Mahasiswa();
        }

        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(new CustomFieldLabel(textFieldNim, "NIM "), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldNama, "Nama Mahasiswa"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldJurusan, "Jurusan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldAlamat, "Alamat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldNim);

        windowFormView.show();
    }
  //  public void showForm(MahasiswaView mahasiswaView) { }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }

                mahasiswa.setNIM(textFieldNim.getCurrentValue());
                mahasiswa.setNamaMahasiswa(textFieldNama.getCurrentValue());
                mahasiswa.setJurusan(textFieldJurusan.getCurrentValue());
                mahasiswa.setAlamat(textFieldAlamat.getCurrentValue());


                getService().getMahasiswaServiceAsync().save(mahasiswa, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }

                        AppClient.showInfoMessage(result.getMessage(), parentView.getPagingToolBar());
                    }
                });
            }
        };
    }
}



