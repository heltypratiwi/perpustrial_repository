package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.User;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtUserService")
public interface GwtUserService {
    Result delete(User user);

    boolean isAuthenticatedUser();

    User doLogin(User user);

    void doLogout();

    User getUserFromSession();

    ArrayList<User> findAll();

    PagingLoadResult<User> find(PagingLoadConfig pagingLoadConfig);

    Result save(User user);
}
