package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.common.CustomField;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface CustomFieldProperties extends PropertyAccess<CustomField>{
    @Editor.Path("value")
    ModelKeyProvider<CustomField> key();

    @Editor.Path("label")
    LabelProvider<CustomField> labelName();
}
