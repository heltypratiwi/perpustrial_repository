package com.library.trial.web.shared;

/**
 * Created by yoggi on 5/8/2017.
 */
public class UploadMessage {
    public static final String UPLOAD_SUCCESS = "success";
    public static final String UPLOAD_FAILED = "failed";
    private Integer successUpload;
    private Integer failedUpload;
    private String message;

    public UploadMessage() {
    }

    public UploadMessage(String message) {
        this.message = message;
    }

    public UploadMessage(Integer successUpload, Integer failedUpload) {
        this.successUpload = successUpload;
        this.failedUpload = failedUpload;
    }

    public Integer getSuccessUpload() {
        return successUpload;
    }

    public void setSuccessUpload(Integer successUpload) {
        this.successUpload = successUpload;
    }

    public Integer getFailedUpload() {
        return failedUpload;
    }

    public void setFailedUpload(Integer failedUpload) {
        this.failedUpload = failedUpload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UploadMessage{" + "successUpload=" + successUpload + ", failedUpload=" + failedUpload + '}';
    }

}
