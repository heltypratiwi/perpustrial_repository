package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Agama;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtAgamaServiceAsync {

    void save(Agama agama, AsyncCallback<Result> async);

    void delete(Agama agama, AsyncCallback<Result> async);

    void find(Agama agama, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Agama>> async);

    void find(Agama agama, AsyncCallback<ArrayList<Agama>> async);
}
