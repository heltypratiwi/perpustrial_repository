package com.library.trial.web.client.view.custom;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.entity.Month;
import com.library.trial.web.client.view.properties.BaseProperties;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;

import java.util.Date;

/**
 * Created by yoggi on 5/16/2017.
 */
public class ComboBoxMonth implements IsWidget {

    private String[] monthName = {"Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

    @Override
    public Widget asWidget() {
        ListStore<Month> listStore = new ListStore<>(BaseProperties.getInstance().getMonthProperties().key());
        ComboBox<Month> comboBox = new ComboBox<>(listStore, BaseProperties.getInstance().getMonthProperties().labelMonthName());

        comboBox.setWidth(90);
        comboBox.setEditable(false);
        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);

        for (int i = 1; i <= 12; i++) {
            listStore.add(new Month (i, monthName[i - 1]));
        }

        comboBox.setValue(listStore.findModelWithKey(new Date().getMonth() + 1 + ""));
        return comboBox;
    }

}
