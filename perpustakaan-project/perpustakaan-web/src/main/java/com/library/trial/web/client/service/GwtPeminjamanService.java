package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Peminjaman;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtPeminjamanService")
public interface GwtPeminjamanService {
    Result save(Peminjaman peminjaman);

    Result delete(Peminjaman peminjaman);

    PagingLoadResult<Peminjaman> find(Peminjaman peminjaman, PagingLoadConfig pagingLoadConfig);

    ArrayList<Peminjaman> find(Peminjaman peminjaman);
}
