package com.library.trial.web.server.service;

import com.library.trial.core.AppCore;
import com.library.trial.web.client.service.GwtSessionHandlerService;
import org.springframework.stereotype.Service;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtSessionHandlerService")
public class SessionHandlerServiceImpl implements GwtSessionHandlerService{

    private int timeout;

    @Override
    public boolean isValidSession() {
        if (!AppCore.getInstance().getHttpServletRequest().isRequestedSessionIdValid()) {
            AppCore.getLogger(this).info("session expired !!! ");
            return false;
        }

        return true;
    }

    @Override
    @Deprecated
    public boolean isSessionAlive() {
        return (System.currentTimeMillis() - AppCore.getInstance().getHttpServletRequest().getSession().getLastAccessedTime()) < timeout;
    }

    @Override
    public Integer getUserSessionTimeout() {
        timeout = AppCore.getInstance().getHttpServletRequest().getSession().getMaxInactiveInterval() * 1000;
        return timeout;
    }
}
