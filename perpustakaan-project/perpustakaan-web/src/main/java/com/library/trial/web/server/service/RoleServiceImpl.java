package com.library.trial.web.server.service;

import com.library.trial.core.entity.Role;
import com.library.trial.core.service.RoleService;
import com.library.trial.web.client.service.GwtRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtRoleService")
public class RoleServiceImpl implements GwtRoleService {
    @Autowired
    private RoleService roleService;

    @Override
    public ArrayList<Role> findAll() {
        return new ArrayList<>(roleService.findAll());
    }

}
