package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.library.trial.core.entity.Agama;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface AgamaProperties extends PropertyAccess<Agama> {
    @Editor.Path("id")
    ModelKeyProvider<Agama> key();

    @Editor.Path("nama")
    LabelProvider<Agama> labelNama();

    @Editor.Path("nama")
    ValueProvider<Agama, String> valueNama();

    @Editor.Path("createdTime")
    ValueProvider<Agama, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Agama, String> valueCreatedBy();


}
