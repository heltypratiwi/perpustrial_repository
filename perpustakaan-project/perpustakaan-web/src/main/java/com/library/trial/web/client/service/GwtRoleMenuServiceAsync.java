package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.RoleMenu;

import java.util.List;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtRoleMenuServiceAsync {
    void save(List<RoleMenu> roleMenus, AsyncCallback<Result> callback);
}
