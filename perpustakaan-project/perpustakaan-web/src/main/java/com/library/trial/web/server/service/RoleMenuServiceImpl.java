package com.library.trial.web.server.service;

import com.library.trial.core.common.Result;
import com.library.trial.core.entity.RoleMenu;
import com.library.trial.core.service.RoleMenuService;
import com.library.trial.web.client.service.GwtRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtRoleMenuService")
public class RoleMenuServiceImpl implements GwtRoleMenuService{
    @Autowired
    private RoleMenuService service;

    @Override
    public Result save(List<RoleMenu> roleMenus) {
        return service.save(roleMenus);
    }
}
