package com.library.trial.web.client.view.custom;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.library.trial.core.AppCore;
import com.library.trial.core.common.DateType;
import com.sencha.gxt.core.client.util.DateWrapper;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;
import com.sencha.gxt.widget.core.client.form.validator.MaxDateValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinDateValidator;

import java.util.Date;

/**
 * @author krissadewo
 */
public class CustomDateField extends DateField {

    public CustomDateField(DateType dateType) {
        this.setEditable(false);
        this.setPropertyEditor(new DateTimePropertyEditor(AppCore.STANDARD_DATE_FORMAT));

        if (dateType != DateType.TANGGAL_LAHIR) {
            this.addValidator(new MinDateValidator(new DateWrapper().getFirstDayOfMonth().asDate()));
        }

        this.addValidator(new MaxDateValidator(new DateWrapper().asDate()));
        this.setValue(new DateWrapper().asDate());

        this.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                setValue(event.getValue());
            }
        });
    }
}
