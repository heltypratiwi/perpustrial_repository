package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Pengembalian;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/15/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtPengembalianService")
public interface GwtPengembalianService {
    Result save(Pengembalian pengembalian);

    Result delete(Pengembalian pengembalian);

    PagingLoadResult<Pengembalian> find(Pengembalian pengembalian, PagingLoadConfig pagingLoadConfig);

    ArrayList<Pengembalian> find(Pengembalian pengembalian);
}
