package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Menu;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface MenuProperties extends PropertyAccess<Menu> {

    @Editor.Path("id")
    ModelKeyProvider<Menu> key();

    @Editor.Path("nama")
    ValueProvider<Menu, String> valueNamaMenu();

    @Editor.Path("roleMenu.canRead")
    ValueProvider<Menu, Boolean> valueCanRead();

    @Editor.Path("roleMenu.canSave")
    ValueProvider<Menu, Boolean> valueCanSave();

    @Editor.Path("roleMenu.canEdit")
    ValueProvider<Menu, Boolean> valueCanEdit();

    @Editor.Path("roleMenu.canDelete")
    ValueProvider<Menu, Boolean> valueCanDelete();

    @Editor.Path("roleMenu.canPrint")
    ValueProvider<Menu, Boolean> valueCanPrint();
}
