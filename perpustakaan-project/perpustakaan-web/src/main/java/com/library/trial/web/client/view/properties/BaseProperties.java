package com.library.trial.web.client.view.properties;

import com.google.gwt.core.client.GWT;

/**
 * Created by yoggi on 5/10/2017.
 */
public class BaseProperties {
    private static BaseProperties instance;
    private final MenuProperties menuProperties = GWT.create(MenuProperties.class);
    private final MonthProperties monthProperties = GWT.create(MonthProperties.class);
    private final RoleProperties roleProperties = GWT.create(RoleProperties.class);
    private final UserProperties userProperties = GWT.create(UserProperties.class);
    private final CustomFieldProperties customFieldProperties = GWT.create(CustomFieldProperties.class);
    private final AgamaProperties agamaProperties = GWT.create(AgamaProperties.class);
    private final BukuProperties bukuProperties = GWT.create(BukuProperties.class);
    private final MahasiswaProperties mahasiswaProperties = GWT.create(MahasiswaProperties.class);
    private final PeminjamanProperties peminjamanProperties = GWT.create(PeminjamanProperties.class);
    private final PengembalianProperties pengembalianProperties = GWT.create(PengembalianProperties.class);

    private BaseProperties() {
    }

    public static BaseProperties getInstance() {
        if (instance == null) {
            instance = new BaseProperties();
        }
        return instance;
    }

    public static void setInstance(BaseProperties instance) {
        BaseProperties.instance = instance;
    }

    public MenuProperties getMenuProperties() {
        return menuProperties;
    }

    public MonthProperties getMonthProperties() {
        return monthProperties;
    }

    public RoleProperties getRoleProperties() {
        return roleProperties;
    }

    public UserProperties getUserProperties() {
        return userProperties;
    }

    public CustomFieldProperties getCustomFieldProperties() {
        return customFieldProperties;
    }

    public BukuProperties getBukuProperties() {
        return bukuProperties;
    }

    public MahasiswaProperties getMahasiswaProperties() {
        return mahasiswaProperties;
    }

    public AgamaProperties getAgamaProperties() {
        return agamaProperties;
    }

    public PeminjamanProperties getPeminjamanProperties() {
        return peminjamanProperties;
    }

    public PengembalianProperties getPengembalianProperties() {
        return pengembalianProperties;
    }
}

