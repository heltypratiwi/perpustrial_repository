package com.library.trial.web.server.service;

import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.service.PeminjamanService;
import com.library.trial.web.client.service.GwtPeminjamanService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtPeminjamanService")
public class PeminjamanServiceImpl implements GwtPeminjamanService {
    @Autowired
    private PeminjamanService peminjamanService;

    @Override
    public Result save(Peminjaman peminjaman) {
        return peminjamanService.save(peminjaman);
    }

    @Override
    public Result delete(Peminjaman peminjaman) {
        return peminjamanService.delete(peminjaman);
    }

    @Override
    public PagingLoadResult<Peminjaman> find(Peminjaman peminjaman, PagingLoadConfig pagingLoadConfig) {
        return new PagingLoadResultBean<>(peminjamanService.find(peminjaman, pagingLoadConfig.getOffset(),
                pagingLoadConfig.getLimit()),
                peminjamanService.count(peminjaman), pagingLoadConfig.getOffset()
        );
    }

    @Override
    public ArrayList<Peminjaman> find(Peminjaman peminjaman) {
        return new ArrayList<>(peminjamanService.find(peminjaman, 0, Integer.MAX_VALUE));
    }
}
