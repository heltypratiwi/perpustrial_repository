package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Pengembalian;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/15/2017.
 */
public interface GwtPengembalianServiceAsync {
    void save(Pengembalian pengembalian, AsyncCallback<Result> callback);

    void delete(Pengembalian pengembalian, AsyncCallback<Result> callback);

    void find(Pengembalian pengembalian, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Pengembalian>> callback);

    void find(Pengembalian pengembalian, AsyncCallback<ArrayList<Pengembalian>> callback);
}
