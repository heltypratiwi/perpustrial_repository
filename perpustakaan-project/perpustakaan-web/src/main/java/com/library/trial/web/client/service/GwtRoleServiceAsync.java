package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.entity.Role;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtRoleServiceAsync {
    void findAll(AsyncCallback<ArrayList<Role>> callback);


}
