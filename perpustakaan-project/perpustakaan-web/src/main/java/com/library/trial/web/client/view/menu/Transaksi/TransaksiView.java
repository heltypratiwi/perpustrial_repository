package com.library.trial.web.client.view.menu.Transaksi;

import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.entity.Menu;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.GridView;
import com.library.trial.web.client.view.handler.GridHandler;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * Created by yoggi on 5/15/2017.
 */
public class TransaksiView extends View implements GridHandler {
    private PeminjamanView peminjamanView;
    private PengembalianView pengembalianView;
    private View view;

    public TransaksiView(Menu menu) {
        super(menu);
}

    public Widget asWidget() {
        peminjamanView = new PeminjamanView(getMenu());
        pengembalianView = new PengembalianView(getMenu());

        VerticalLayoutContainer verticalLayoutContainerRight = new VerticalLayoutContainer();
        verticalLayoutContainerRight.add(peminjamanView, new VerticalLayoutContainer.VerticalLayoutData(-1, 1));

        VerticalLayoutContainer verticalLayoutContainerLeft = new VerticalLayoutContainer();
        verticalLayoutContainerLeft.add(pengembalianView, new VerticalLayoutContainer.VerticalLayoutData(-1, 1));

        BoxLayoutContainer.BoxLayoutData flex = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 0, 0));
        flex.setFlex(1);

        /*HBoxLayoutContainer hBoxLayoutContainer = new HBoxLayoutContainer();
        hBoxLayoutContainer.add(verticalLayoutContainerRight, new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 5)));
        hBoxLayoutContainer.add(verticalLayoutContainerLeft, new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 0)));*/

        HorizontalLayoutContainer hBoxLayoutContainer = new HorizontalLayoutContainer();
        hBoxLayoutContainer.add(verticalLayoutContainerRight, new HorizontalLayoutContainer.HorizontalLayoutData(-1, 1, new Margins(5, 5, 5, 5)));
        hBoxLayoutContainer.add(verticalLayoutContainerLeft, new HorizontalLayoutContainer.HorizontalLayoutData(-1, -1, new Margins(5, 5, 5, 5)));

        GridView gridView = new GridView(getMenu());
        gridView.addWidget(hBoxLayoutContainer, new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        return gridView;
    }


    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return null;
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return null;
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return null;
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return null;
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return null;
    }

    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return null;
    }
}
