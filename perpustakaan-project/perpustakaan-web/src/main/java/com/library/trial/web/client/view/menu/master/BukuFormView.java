package com.library.trial.web.client.view.menu.master;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.CustomFieldLabel;
import com.library.trial.web.client.view.custom.WindowFormView;
import com.library.trial.web.client.view.handler.FormHandler;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Created by yoggi on 5/10/2017.
 */
public class BukuFormView extends View implements FormHandler{

    private WindowFormView windowFormView ;
    private BukuView parentView;
    private TextField textFieldKode;
    private TextField textFieldJudul;
    private TextField textFieldIsbn;
    private Buku buku;
    @Override
    public void showForm(View view) {
        parentView = (BukuView) view;
        textFieldKode = new TextField();
        textFieldJudul = new TextField();
        textFieldIsbn = new TextField();

        textFieldKode.setAllowBlank(false);
        textFieldJudul.setAllowBlank(false);
        textFieldIsbn.setAllowBlank(false);


        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            buku = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldKode.setValue(buku.getKode());
            textFieldJudul.setValue(buku.getJudul());
            textFieldIsbn.setValue(buku.getIsbn());

        } else {
            buku = new Buku();
        }

        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(new CustomFieldLabel(textFieldKode, "Kode"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldJudul, "Judul"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldIsbn, "Isbn"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldKode);

        windowFormView.show();
    }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }
                buku.setKode(textFieldKode.getCurrentValue());
                buku.setJudul(textFieldJudul.getCurrentValue());
                buku.setIsbn(textFieldIsbn.getCurrentValue());


                getService().getBukuServiceAsync().save(buku, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }
                        AppClient.showInfoMessage(result.getMessage(), parentView.getPagingToolBar());
                    }
                });

            }
        };
    }

}
