package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Mahasiswa;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface MahasiswaProperties extends PropertyAccess<Mahasiswa>{
    @Editor.Path("id")
    ModelKeyProvider<Mahasiswa> key();

    @Editor.Path("NIM")
    LabelProvider<Mahasiswa> labelNIM();

    @Editor.Path("NIM")
    ValueProvider<Mahasiswa, String> valueNIM();

    @Editor.Path("Nama")
    LabelProvider<Mahasiswa> labelNama();

    @Editor.Path("Nama")
    ValueProvider<Mahasiswa, String> valueNama();

    @Editor.Path("Jurusan")
    LabelProvider<Mahasiswa> labelJurusan();

    @Editor.Path("Jurusan")
    ValueProvider<Mahasiswa, String> valueJurusan();

    @Editor.Path("Alamat")
    LabelProvider<Mahasiswa> labelAlamat();

    @Editor.Path("Alamat")
    ValueProvider<Mahasiswa, String> valueAlamat();

    @Editor.Path("createdTime")
    ValueProvider<Mahasiswa, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Mahasiswa, String> valueCreatedBy();
}
