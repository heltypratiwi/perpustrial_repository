package com.library.trial.web.server.service;

import com.library.trial.core.common.Result;
import com.library.trial.core.entity.User;
import com.library.trial.core.service.UserService;
import com.library.trial.web.client.service.GwtUserService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service("gwtUserService")
public class UserServiceImpl implements GwtUserService {

    @Autowired
    private UserService service;

    @Override
    public boolean isAuthenticatedUser() {
        return service.isAuthenticatedUser();
    }

    @Override
    public User doLogin(User user) {
        return service.doLogin(user);
    }

    @Override
    public void doLogout() {
        service.doLogout();
    }

    @Override
    public User getUserFromSession() {
        return service.getUserFromSession();
    }

    @Override
    public ArrayList<User> findAll() {
        return new ArrayList<User>(service.getAll());
    }

    @Override
    public PagingLoadResult<User> find(PagingLoadConfig pagingLoadConfig) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Result save(User user) {
        return service.save(user);
    }

    @Override
    public Result delete(User user) {
        return service.delete(user);
    }
}
