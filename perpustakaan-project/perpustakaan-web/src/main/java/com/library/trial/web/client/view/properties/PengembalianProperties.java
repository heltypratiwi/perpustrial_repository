package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.entity.Pengembalian;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by yoggi on 5/15/2017.
 */
public interface PengembalianProperties extends PropertyAccess<Pengembalian> {
    @Editor.Path("id")
    ModelKeyProvider<Pengembalian> key();

    @Editor.Path("tanggalPinjam")
    LabelProvider<Peminjaman> labelKodePinjam();

    @Editor.Path("buku.judul")
    ValueProvider<Pengembalian, String> valueIdBuku();

    @Editor.Path("mahasiswa.nim")
    ValueProvider<Pengembalian, String> valueIdMahasiswa();

    @Editor.Path("tanggalKembali")
    ValueProvider<Pengembalian, Date> valueTanggalKembali();

    @Editor.Path("peminjaman.tanggalPinjam")
    ValueProvider<Pengembalian, Date> valueTanggalPinjam();

    @Editor.Path("createdTime")
    ValueProvider<Pengembalian, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Pengembalian, String> valueCreatedBy();
}
