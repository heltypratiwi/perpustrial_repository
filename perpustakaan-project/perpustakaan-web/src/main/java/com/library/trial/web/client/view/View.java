package com.library.trial.web.client.view;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.core.entity.Menu;
import com.library.trial.web.client.service.GwtBaseService;
import com.library.trial.web.client.view.handler.FormHandler;
import com.library.trial.web.client.view.properties.BaseProperties;

/**
 * Created by yoggi on 5/10/2017.
 */
public abstract class View implements IsWidget{

    //################# MASTER #################################
    public static final String VIEW_MASTER_MAHASISWA = "VIEW_MASTER_MAHASISWA";
    public static final String VIEW_MASTER_BUKU = "VIEW_MASTER_BUKU";
    public static final String VIEW_MASTER_AGAMA = "VIEW_MASTER_AGAMA";


    //######################## SETTING ###############################
    public static final String VIEW_SETTING_HAK_AKSES = "VIEW_SETTING_HAK_AKSES";
    public static final String VIEW_SETTING_USER_MANAGEMENT = "VIEW_SETTING_USER_MANAGEMENT";
    public static final String TRANSAKSI_PEMINJAMAN = "TRANSAKSI_PEMINJAMAN";
    public static final String TRANSAKSI_PENGEMBALIAN = "TRANSAKSI_PENGEMBALIAN";
    public static final String TRANSAKSI_VIEW = "TRANSAKSI_VIEW";
    private Menu menu;

    public View(){}

    public View(Menu menu){
        this.menu = menu;
    }

    public View(com.sencha.gxt.widget.core.client.menu.Menu menu) {

    }

    //  public View(Menu menu) {
   // }

    public static Widget loadView(View view) {
        return view.asWidget();
    }

    public static native int getScreenWidth() /*-{
        return $wnd.screen.width;
    }-*/;

    public static native int getScreenHeight() /*-{
        return $wnd.screen.height;
    }-*/;

    @Override
    public Widget asWidget() {
        return asWidget();
    }

    /**
     * All about RPC service will be use on client application
     *
     * @return service
     */
    protected GwtBaseService getService() {
        return GwtBaseService.getInstance();
    }

    protected BaseProperties getProperties() {
        return BaseProperties.getInstance();
    }

    /**
     * Override this method with specific name of view. View name will be used
     * to selection for what view will be used when tree menu has changed
     *
     * @return
     */
    public Menu getMenu() {
        return menu;
    }

    public void registerShowEvent(final FormHandler formHandler, final View view) {
        Event.addNativePreviewHandler(new Event.NativePreviewHandler() {
            @Override
            public void onPreviewNativeEvent(Event.NativePreviewEvent event) {
                if (event.getTypeInt() == Event.ONKEYDOWN) {
                    if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_INSERT) {
                        formHandler.showForm(view);
                    }
                }
            }
        });
    }



}

