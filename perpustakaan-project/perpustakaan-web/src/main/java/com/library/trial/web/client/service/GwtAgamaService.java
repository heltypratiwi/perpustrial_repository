package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Agama;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtAgamaService")
public interface GwtAgamaService extends RemoteService{

    Result save(Agama agama);

    Result delete(Agama agama);

    PagingLoadResult<Agama> find(Agama agama, PagingLoadConfig pagingLoadConfig);

    ArrayList<Agama> find(Agama agama);
}
