package com.library.trial.web.shared;

/**
 * Created by yoggi on 5/8/2017.
 */
public class FieldVerifier {
    public static boolean isValidName(String name) {
        if (name == null) {
            return false;
        }
        return name.length() > 3;
    }
}
