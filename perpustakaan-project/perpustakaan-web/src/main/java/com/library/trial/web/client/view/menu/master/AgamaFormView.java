package com.library.trial.web.client.view.menu.master;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Agama;
import com.library.trial.web.client.AppClient;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.CustomFieldLabel;
import com.library.trial.web.client.view.custom.WindowFormView;
import com.library.trial.web.client.view.handler.FormHandler;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Created by yoggi on 5/10/2017.
 */
public class AgamaFormView extends View implements FormHandler{
    private WindowFormView windowFormView ;
    private AgamaView parentView;
    private TextField textFieldNama;
    private Agama agama;

    @Override
    public void showForm(View view) {
        parentView = (AgamaView) view;
        textFieldNama = new TextField();
        textFieldNama.setAllowBlank(false);

        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            agama = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldNama.setValue(agama.getNama());
        } else {
            agama = new Agama();
        }

        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(new CustomFieldLabel(textFieldNama, "Nama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldNama);

        windowFormView.show();
    }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }

                agama.setNama(textFieldNama.getCurrentValue());

                getService().getAgamaServiceAsync().save(agama, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        AppClient.showMessageOnFailureException(throwable);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }

                        AppClient.showInfoMessage(result.getMessage(), parentView.getPagingToolBar());
                    }
                });
            }
        };
    }
}
