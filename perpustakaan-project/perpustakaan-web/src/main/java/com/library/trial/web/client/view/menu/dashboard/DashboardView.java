package com.library.trial.web.client.view.menu.dashboard;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.library.trial.web.client.view.View;
import com.library.trial.web.client.view.custom.GridView;
import com.library.trial.web.client.view.custom.WindowSearchView;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;

/**
 * Created by yoggi on 5/10/2017.
 */
public class DashboardView extends View{
    private FlowPanel flowPanel;
    private WindowSearchView windowSearchView;

    @Override
    public Widget asWidget() {
        flowPanel = new FlowPanel();


        GridView gridView = new GridView();
        gridView.setHeaderVisible(false);

        gridView.addWidget(flowPanel, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        return gridView;
    }
}
