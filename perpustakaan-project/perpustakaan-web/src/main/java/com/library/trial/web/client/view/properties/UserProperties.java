package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.User;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface UserProperties extends PropertyAccess<User>{
    @Editor.Path("id")
    ModelKeyProvider<User> key();

    @Editor.Path("realname")
    ValueProvider<User, String> valueRealName();

    @Editor.Path("username")
    LabelProvider<User> labelUsername();

    @Editor.Path("realname")
    LabelProvider<User> labelRealname();

    @Editor.Path("username")
    ValueProvider<User, String> valueUsername();

    @Editor.Path("password")
    ValueProvider<User, String> valuePassword();

    @Editor.Path("role.nama")
    ValueProvider<User, String> valueNamaRole();

}
