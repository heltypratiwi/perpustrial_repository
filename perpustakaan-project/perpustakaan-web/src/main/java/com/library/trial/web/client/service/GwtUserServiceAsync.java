package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.User;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GwtUserServiceAsync {
    void delete(User user, AsyncCallback<Result> callback);

    void isAuthenticatedUser(AsyncCallback<Boolean> callback);

    void doLogin(User user, AsyncCallback<User> callback);

    void doLogout(AsyncCallback<Void> callback);

    void getUserFromSession(AsyncCallback<User> callback);

    void findAll(AsyncCallback<ArrayList<User>> callback);

    void find(PagingLoadConfig config, AsyncCallback<PagingLoadResult<User>> callback);

    void save(User user, AsyncCallback<Result> callback);
}
