package com.library.trial.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.library.trial.core.common.Result;
import com.library.trial.core.entity.Buku;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by yoggi on 5/10/2017.
 */
@RemoteServiceRelativePath("springGwtServices/gwtBukuService")
public interface GwtBukuService extends RemoteService {
    Result save (Buku buku);

    Result delete (Buku buku);

    PagingLoadResult<Buku> find(Buku buku, PagingLoadConfig pagingLoadConfig);

    ArrayList<Buku> find(Buku buku);

    Buku findByKode(Buku buku);
}
