package com.library.trial.web.client.view.handler;

import com.library.trial.web.client.view.View;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface FormHandler {
    void showForm(View view);

    SelectEvent.SelectHandler buttonSaveSelectHandler();
}
