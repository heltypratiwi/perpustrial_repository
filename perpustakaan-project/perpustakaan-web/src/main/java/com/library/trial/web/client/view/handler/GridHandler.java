package com.library.trial.web.client.view.handler;

import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface GridHandler {
    SelectHandler buttonAddSelectHandler();

    SelectHandler buttonEditSelectHandler();

    SelectHandler buttonDeleteSelectHandler();

    SelectHandler buttonSearchSelectHandler();

    SelectHandler buttonPrintSelectHandler();

    RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler();
}
