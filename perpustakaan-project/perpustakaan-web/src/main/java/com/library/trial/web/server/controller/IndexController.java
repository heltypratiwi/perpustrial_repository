package com.library.trial.web.server.controller;

import com.library.trial.core.entity.Buku;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.Map;

/**
 * Created by yoggi on 5/10/2017.
 */
@Controller
public class IndexController extends BaseController{
    @RequestMapping(value = "/index")
    public ModelAndView showIndex(ModelAndView modelAndView) {
        modelAndView.addObject("date", new Date());
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/test", produces = "application/json")
    public Map<String, Object> find() {
        return convertModel(new Buku(), HttpStatus.OK);
    }

}
