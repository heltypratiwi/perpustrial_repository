package com.library.trial.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.library.trial.core.entity.Role;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface RoleProperties extends PropertyAccess<Role> {

    @Editor.Path("id")
    ModelKeyProvider<Role> key();

    @Editor.Path("nama")
    ModelKeyProvider<Role> valueNama();

    @Editor.Path("nama")
    LabelProvider<Role> labelNama();
}
