package com.library.trial.core.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Constant;
import com.library.trial.core.common.Result;
import com.library.trial.core.dao.MahasiswaDAO;
import com.library.trial.core.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service
public class MahasiswaService extends BaseService {
    @Autowired
    public MahasiswaDAO mahasiswaDAO;

    public Result save(final Mahasiswa mahasiswa) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    mahasiswa.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (mahasiswa.getId() == null) {
                        mahasiswaDAO.save(mahasiswa);
                    } else {
                        mahasiswaDAO.update(mahasiswa);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Mahasiswa mahasiswa) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    mahasiswa.setNIM(mahasiswa.getNIM().concat(Constant.FLAG_DELETE));
                    mahasiswa.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    mahasiswaDAO.delete(mahasiswa);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Mahasiswa findById(Long id) {
        return mahasiswaDAO.findById(id);
    }

    public List<Mahasiswa> find(Mahasiswa mahasiswa, int offset, int limit) {
        return mahasiswaDAO.find(mahasiswa, offset, limit);
    }

    public int count(Mahasiswa mahasiswa) {
        return mahasiswaDAO.count(mahasiswa);
    }
}
