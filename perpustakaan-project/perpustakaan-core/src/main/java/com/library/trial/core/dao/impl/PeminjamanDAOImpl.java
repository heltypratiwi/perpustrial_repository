package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.PeminjamanDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.entity.User;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoggi on 5/10/2017.
 */
    @Repository
    public class PeminjamanDAOImpl implements PeminjamanDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Peminjaman save(Peminjaman entity) {
        String sql = "INSERT INTO " + Table.MASTER_PEMINJAMAN + " (" +
                "kode_buku, " +
                "nim_mahasiswa," +
                "tanggal_pinjam," +
                "created_by," +
                "created_time) " +
                "VALUES(?,?,?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getBuku().getId(),
                entity.getMahasiswa().getId(),

                new Timestamp(entity.getTanggalPinjam().getTime()),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()));
        return entity;
    }

    @Override
    public Peminjaman update(Peminjaman entity) {
        String sql = "UPDATE " + Table.MASTER_PEMINJAMAN + " SET " +
                "kode_buku = ? , " +
                "nim_mahasiswa = ? , " +
                "tanggal_pinjam = ?," +
                "created_by = ? ," +
                "created_time = ? " +
                "WHERE id =  ? ";

        jdbcTemplate.update(sql,
                entity.getBuku().getId(),
                entity.getMahasiswa().getId(),

                new Timestamp(entity.getTanggalPinjam().getTime()),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()),
                entity.getId());
        return entity;
    }

    @Override
    public Peminjaman delete(Peminjaman entity) {
        String sql = "DELETE FROM " + Table.MASTER_PEMINJAMAN+ " WHERE id =  ?";

        jdbcTemplate.update(sql, entity.getId());
        return entity;
    }

    @Override
    public Peminjaman findById(long id) {
        String sql = "SELECT * FROM " + Table.MASTER_PEMINJAMAN + " a " +
                "INNER JOIN " + Table.MASTER_BUKU + " b ON b.id = a.id_buku " +
                "INNER JOIN " + Table.MASTER_MAHASISWA + " c ON c.id = a.id_mahasiswa " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 " +
                "AND a.id = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new PeminjamanRowMapper(), id);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return null;
    }

    @Override
    public List<Peminjaman> find(Peminjaman param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.MASTER_PEMINJAMAN + " a " +
                "INNER JOIN " + Table.MASTER_BUKU + " b ON b.id = a.id_buku " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "INNER JOIN " + Table.MASTER_MAHASISWA + " c ON c.id = a.id_mahasiswa " +
                "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getTanggalPinjam() != null) {
            params.add("%" + param.getTanggalPinjam() + "%");
            sql += "AND tanggal_pinjam = ? ";
        }
        return jdbcTemplate.query(sql, params.toArray(), new PeminjamanRowMapper());
    }

    @Override
    public int count(Peminjaman param) {
        String sql = "SELECT COUNT(id) FROM " + Table.MASTER_PEMINJAMAN+ " WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getTanggalPinjam() != null) {
            params.add("%" + param.getTanggalPinjam() + "%");
            sql += " AND tanggal_pinjam = ? ";
        }

        return jdbcTemplate.queryForObject(sql, params.toArray(), Integer.class);
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    class PeminjamanRowMapper implements RowMapper<Peminjaman> {
        @Override
        public Peminjaman mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setRealname(rs.getString("realname"));

            Buku buku = new Buku();
            buku.setId(rs.getLong("id"));
            buku.setKode(rs.getString("kode"));
            buku.setJudul(rs.getString("judul"));
            buku.setIsbn(rs.getString("isbn"));
            buku.setNomerRak(rs.getString("nomer_rak"));


            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setId(rs.getLong("id"));
            mahasiswa.setNIM(rs.getString("nim"));
            mahasiswa.setNamaMahasiswa(rs.getString("nama"));
            mahasiswa.setJurusan(rs.getString("jurusan"));

            Peminjaman peminjaman = new Peminjaman();
            peminjaman.setId(rs.getLong("id"));
            peminjaman.setBuku(buku);
            peminjaman.setMahasiswa(mahasiswa);
            peminjaman.setTanggalPinjam(rs.getTimestamp("tanggal_pinjam"));
            peminjaman.setCreatedBy(user);
            peminjaman.setCreatedTime(rs.getTimestamp("created_time"));
            return peminjaman;
        }
    }
}
