package com.library.trial.core.dao;

import com.library.trial.core.entity.Role;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface RoleDAO extends BaseDAO<Role> {
}
