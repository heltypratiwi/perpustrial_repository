package com.library.trial.core.common;

/**
 * Created by yoggi on 5/5/2017.
 */
public class Pattern {

    public static final String YYYY_DD_MM_WITH_STRIP = "yyyy-dd-mm";
    public static final String DD_MM_YYYY_WITH_STRIP = "dd-MM-yyyy";
    public static final String YYYY_MM_DD_WITH_BACK_SLASH = "yyyy/MM/dd";
    public static final String DD_MM_YYYY_WITH_BACK_SLASH = "dd/MM/yyyy";
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy hh:mm:ss";

}
