package com.library.trial.core.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Constant;
import com.library.trial.core.common.Result;
import com.library.trial.core.dao.AgamaDAO;
import com.library.trial.core.entity.Agama;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by yoggi on 5/12/2017.
 */
public class AgamaService extends BaseService {
    @Autowired
    private AgamaDAO agamaDAO;

    public Result save(final Agama agama) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    agama.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (agama.getId() == null) {
                        agamaDAO.save(agama);
                    } else {
                        agamaDAO.update(agama);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Agama agama) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    agama.setNama(agama.getNama().concat(Constant.FLAG_DELETE));
                    agama.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    agamaDAO.delete(agama);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Agama findById(Long id) {
        return agamaDAO.findById(id);
    }

    public List<Agama> find(Agama agama, int offset, int limit) {
        return agamaDAO.find(agama, offset, limit);
    }

    public int count(Agama agama) {
        return agamaDAO.count(agama);
    }

}
