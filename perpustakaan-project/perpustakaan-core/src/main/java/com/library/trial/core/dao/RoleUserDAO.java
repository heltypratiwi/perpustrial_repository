package com.library.trial.core.dao;

import com.library.trial.core.entity.RoleUser;
import com.library.trial.core.entity.User;

import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface RoleUserDAO extends BaseDAO<RoleUser>{

    List<RoleUser> save(List<RoleUser> roleUsers);

    List<RoleUser> find(User user);

    /**
     * Delete role user by id users
     *
     * @param roleUsers list of role user
     */
    void delete(List<RoleUser> roleUsers);
}
