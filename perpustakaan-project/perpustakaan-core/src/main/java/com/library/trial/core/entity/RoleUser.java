package com.library.trial.core.entity;

import java.io.Serializable;

/**
 * Created by yoggi on 5/5/2017.
 */
public class RoleUser implements Serializable{
    private static final long serialVersionUID = -153758805335656075L;
    private Long id;
    private User user;
    private Role role;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RoleUser{" +
                "id=" + id +
                ", user=" + user +
                ", role=" + role +
                '}';
    }
}
