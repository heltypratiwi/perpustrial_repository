package com.library.trial.core.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Result;
import com.library.trial.core.dao.BukuDAO;
import com.library.trial.core.dao.PengembalianDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Pengembalian;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by yoggi on 5/15/2017.
 */
public class PengembalianService extends BaseService {
    @Autowired
    private PengembalianDAO pengembalianDAO;
    @Autowired
    private BukuDAO bukuDAO;

    public Result save(final Pengembalian pengembalian) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    pengembalian.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (pengembalian.getId() == null) {
                        pengembalianDAO.save(pengembalian);
                    } else {
                        pengembalianDAO.update(pengembalian);
                        updateBuku(pengembalian);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }
                return null;
            }
        });
    }

    public Result delete(final Pengembalian pengembalian) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    // peminjaman.setId(peminjaman.getId().concat(Constant.FLAG_DELETE));
                    pengembalian.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    pengembalianDAO.delete(pengembalian);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }
                return null;
            }
        });
    }

    public Pengembalian findById(Long id) {
        return pengembalianDAO.findById(id);
    }

    public List<Pengembalian> find(Pengembalian pengembalian, int offset, int limit) {
        return pengembalianDAO.find(pengembalian, offset, limit);
    }

    public int count(Pengembalian pengembalian) {
        return pengembalianDAO.count(pengembalian);
    }

    /**
     * proses untuk mengupdate jumlah buku
     *
     * @param pengembalian
     */
    private void updateBuku(Pengembalian pengembalian) {
        Buku buku = pengembalian.getBuku();
        buku.setJumlah_tersedia(buku.getJumlah_tersedia() + 1);
        buku.setJumlah_dipinjam(buku.getJumlah_dipinjam() - 1);

        bukuDAO.update(buku);
    }

}
