package com.library.trial.core.dao;

import com.library.trial.core.entity.Buku;

import java.util.List;
/**
 * Created by yoggi on 5/5/2017.
 */
public interface BaseDAO<T> {

    T save(final T entity);

    T update(final T entity);

    T delete(final T entity);

    T findById(final long id);

    List<T> find(T param, Integer offset, Integer limit);


    int count(T param);

    Buku findByKode(Buku buku);
}
