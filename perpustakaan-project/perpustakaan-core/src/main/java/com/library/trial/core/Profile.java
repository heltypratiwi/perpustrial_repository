package com.library.trial.core;

/**
 * Created by yoggi on 5/5/2017.
 */
public class Profile {

    private String uploadLocation;
    private String name;
    private String appTittle;

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppTittle() {
        return appTittle;
    }

    public void setAppTittle(String appTittle) {
        this.appTittle = appTittle;

    }

    public static enum STATUS {
        PROD, DEV
    }
}
