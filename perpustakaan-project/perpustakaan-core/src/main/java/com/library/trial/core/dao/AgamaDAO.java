package com.library.trial.core.dao;

import com.library.trial.core.entity.Agama;

/**
 * Created by yoggi on 5/12/2017.
 */
public interface AgamaDAO extends BaseDAO<Agama> {
}
