package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.SystemParameterDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
    @Repository
    public class SystemParameterDAOImpl implements SystemParameterDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public SystemParameter save(SystemParameter entity) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SystemParameter update(SystemParameter entity) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SystemParameter delete(SystemParameter entity) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SystemParameter findById(long id) {
        return null;
    }



    @Override
    public List<SystemParameter> find(SystemParameter param, Integer offset, Integer limit) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int count(SystemParameter paramWrapper) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    @Override
    public SystemParameter get() {
        String sql = "SELECT *FROM system_parameter";
        return jdbcTemplate.queryForObject(sql, new SystemParameterRowMapper());
    }

    class SystemParameterRowMapper implements RowMapper<SystemParameter> {

        @Override
        public SystemParameter mapRow(ResultSet rs, int rowNum) throws SQLException {
            SystemParameter systemParameter = new SystemParameter();
            systemParameter.setReportServer(rs.getString("report_server"));
            return systemParameter;
        }
    }
}
