package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.RoleMenuDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.RoleMenu;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
    @Repository
    public class RoleMenuDAOImpl implements RoleMenuDAO{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @CacheEvict(value = "menu")
    public RoleMenu save(RoleMenu entity) {
        String sql = "INSERT INTO " + Table.SYSTEM_ROLE_MENU + "("
                + "menu_id,"
                + "role_id,"
                + "can_read,"
                + "can_save,"
                + "can_edit,"
                + "can_delete, "
                + "can_print)"
                + "VALUES(?,?,?,?,?,?,?)";

        jdbcTemplate.update(sql,
                entity.getMenu().getId(),
                entity.getRole().getId(),
                entity.isCanRead(),
                entity.isCanSave(),
                entity.isCanEdit(),
                entity.isCanDelete(),
                entity.isCanPrint());

        return entity;
    }

    @Override
    @CacheEvict(value = "menu")
    public RoleMenu update(RoleMenu entity) {
        String sql = "UPDATE " + Table.SYSTEM_ROLE_MENU + " SET "
                + "menu_id = ?,"
                + "role_id = ?,"
                + "can_read = ?,"
                + "can_save = ?, "
                + "can_edit = ?,"
                + "can_delete = ?, "
                + "can_print = ? "
                + "WHERE id = ? ";

        jdbcTemplate.update(sql,
                entity.getMenu().getId(),
                entity.getRole().getId(),
                entity.isCanRead(),
                entity.isCanSave(),
                entity.isCanEdit(),
                entity.isCanDelete(),
                entity.isCanPrint(),
                entity.getId());

        return entity;
    }

    @Override
    public RoleMenu delete(RoleMenu entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RoleMenu findById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        @Override
    public List<RoleMenu> find(RoleMenu param, Integer offset, Integer limit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count(RoleMenu entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }
}
