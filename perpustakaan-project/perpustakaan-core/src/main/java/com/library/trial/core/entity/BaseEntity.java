package com.library.trial.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yoggi on 5/5/2017.
 */
public class BaseEntity implements Serializable{

    private static final long serialVersionUID = -8528633195887266795L;
    private long id;
    private User createdBy;
    private Date createdTime = new Date();
    private User deletedBy;
    private Date deletedTime = new Date();
    private String searchValue;
    private String searchKey;
    private Date searchStartDate;
    private Date searchEndDate;

    public BaseEntity() {

    }

    public BaseEntity(Long id){
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public Date getSearchStartDate() {
        return searchStartDate;
    }

    public void setSearchStartDate(Date searchStartDate) {
        this.searchStartDate = searchStartDate;
    }

    public Date getSearchEndDate() {
        return searchEndDate;
    }

    public void setSearchEndDate(Date searchEndDate) {
        this.searchEndDate = searchEndDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        if (id != that.id) return false;
        if (!createdBy.equals(that.createdBy)) return false;
        if (!createdTime.equals(that.createdTime)) return false;
        if (!deletedBy.equals(that.deletedBy)) return false;
        if (!deletedTime.equals(that.deletedTime)) return false;
        if (!searchValue.equals(that.searchValue)) return false;
        if (!searchKey.equals(that.searchKey)) return false;
        if (!searchStartDate.equals(that.searchStartDate)) return false;
        return searchEndDate.equals(that.searchEndDate);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + createdBy.hashCode();
        result = 31 * result + createdTime.hashCode();
        result = 31 * result + deletedBy.hashCode();
        result = 31 * result + deletedTime.hashCode();
        result = 31 * result + searchValue.hashCode();
        result = 31 * result + searchKey.hashCode();
        result = 31 * result + searchStartDate.hashCode();
        result = 31 * result + searchEndDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                ", createdBy=" + createdBy +
                ", createdTime=" + createdTime +
                ", deletedBy=" + deletedBy +
                ", deletedTime=" + deletedTime +
                ", searchValue='" + searchValue + '\'' +
                ", searchKey='" + searchKey + '\'' +
                ", searchStartDate=" + searchStartDate +
                ", searchEndDate=" + searchEndDate +
                '}';
    }
}
