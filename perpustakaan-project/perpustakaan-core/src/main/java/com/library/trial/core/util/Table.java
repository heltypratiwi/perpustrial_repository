package com.library.trial.core.util;

/**
 * Created by yoggi on 5/8/2017.
 */
public class Table {

    public static final String MASTER_MAHASISWA = "master_mahasiswa ";
    public static final String MASTER_BUKU = "master_buku ";
    public static final String MASTER_AGAMA = "master_agama ";
    public static final String MASTER_PEMINJAMAN = "master_peminjaman ";


    public static final String SYSTEM_MENU = "system_menu ";
    public static final String SYSTEM_ROLE = "system_role ";
    public static final String SYSTEM_USER = "system_user ";
    public static final String SYSTEM_ROLE_USER = "system_role_user ";
    public static final String SYSTEM_ROLE_MENU = "system_role_menu ";


    public static final String LOG_USER = "log_user ";
}
