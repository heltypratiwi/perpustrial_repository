package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.RoleDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Role;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
    @Repository
    public class RoleDAOImpl implements RoleDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Role save(Role entity) {
        String sql = "INSERT INTO " + Table.SYSTEM_ROLE + "(nama) VALUES(?)";

        jdbcTemplate.update(sql, entity.getNama());
        return entity;
    }

    @Override
    public Role update(Role entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Role delete(Role entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Role findById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Role> find(Role param, Integer offset, Integer limit) {
        String sql = "SELECT *FROM " + Table.SYSTEM_ROLE;

        return jdbcTemplate.query(sql, new RoleRowMapper());
    }



    @Override
    public int count(Role entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    class RoleRowMapper implements RowMapper<Role> {

        @Override
        public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
            Role role = new Role();
            role.setId(rs.getLong("id"));
            role.setNama(rs.getString("nama"));
            return role;
        }
    }
}
