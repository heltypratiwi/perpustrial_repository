package com.library.trial.core.dao;

import com.library.trial.core.entity.Mahasiswa;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface MahasiswaDAO extends BaseDAO<Mahasiswa>{

}
