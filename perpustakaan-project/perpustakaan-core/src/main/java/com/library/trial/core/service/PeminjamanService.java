package com.library.trial.core.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Result;
import com.library.trial.core.dao.BukuDAO;
import com.library.trial.core.dao.PeminjamanDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Peminjaman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by yoggi on 5/10/2017.
 */
@Service
public class PeminjamanService extends BaseService {
    @Autowired
    private PeminjamanDAO peminjamanDAO;

    @Autowired BukuDAO bukuDAO;

    public Result save(final Peminjaman peminjaman) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    peminjaman.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (peminjaman.getId() == null) {
                        peminjamanDAO.save(peminjaman);
                        updateBuku(peminjaman);
                    } else {
                        peminjamanDAO.update(peminjaman);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Peminjaman peminjaman) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                   // peminjaman.setBuku(peminjaman.getBuku().concat(Constant.FLAG_DELETE));
                    peminjaman.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    peminjamanDAO.delete(peminjaman);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Peminjaman findById(Long id) {
        return peminjamanDAO.findById(id);
    }

    public List<Peminjaman> find(Peminjaman peminjaman, int offset, int limit) {
        return peminjamanDAO.find(peminjaman, offset, limit);
    }

    public int count(Peminjaman peminjaman) {
        return peminjamanDAO.count(peminjaman);
    }

    /**
     * proses untuk mengupdate jumlah buku
     *
     * @param peminjaman
     */
    private void updateBuku(Peminjaman peminjaman) {
        Buku buku = peminjaman.getBuku();
        buku.setJumlah_tersedia(buku.getJumlah_tersedia() + 1);
        buku.setJumlah_dipinjam(buku.getJumlah_dipinjam() - 1);

        bukuDAO.update(buku);
    }
}
