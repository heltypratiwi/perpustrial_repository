package com.library.trial.core.entity;

import java.io.Serializable;

/**
 * Created by yoggi on 5/5/2017.
 */
public class SystemParameter implements Serializable{
    private static final long serialVersionUID = -5810894004862398878L;
    private String reportServer;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getReportServer() {
        return reportServer;
    }

    public void setReportServer(String reportServer) {
        this.reportServer = reportServer;
    }

    @Override
    public String toString() {
        return "SystemParameter{" +
                "reportServer='" + reportServer + '\'' +
                '}';
    }
}
