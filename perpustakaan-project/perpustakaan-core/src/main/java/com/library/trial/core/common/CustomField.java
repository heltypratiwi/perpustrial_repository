package com.library.trial.core.common;

import java.io.Serializable;

/**
 * Created by yoggi on 5/5/2017.
 */
public class CustomField implements Serializable {

    private static final long serialVersionUID = 3303375556567645094L;
    private String value;
    private int valueInteger;
    private String label;

    public CustomField() {
    }

    public CustomField(String value) {
        this.value = value;
    }

    public CustomField(String valueString, String label) {
        this.value = valueString;
        this.label = label;
    }

    public CustomField(int valueInteger, String label) {
        this.valueInteger = valueInteger;
        this.label = label;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getValueInteger() {
        return valueInteger;
    }

    public void setValueInteger(int valueInteger) {
        this.valueInteger = valueInteger;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "CustomField{" +
                "value='" + value + '\'' +
                ", label='" + label + '\'' +
                '}';
    }
}
