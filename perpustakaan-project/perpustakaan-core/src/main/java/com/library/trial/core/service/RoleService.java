package com.library.trial.core.service;

import com.library.trial.core.dao.RoleDAO;
import com.library.trial.core.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
@Service
public class RoleService {
    @Autowired
    private RoleDAO roleDAO;

    public List<Role> findAll() {
        return roleDAO.find(null, null, null);
    }


}
