package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.PengembalianDAO;
import com.library.trial.core.entity.*;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoggi on 5/15/2017.
 */
@Repository
public class PengembalianDAOImpl implements PengembalianDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Pengembalian save(Pengembalian entity) {
        String sql = "INSERT INTO " + Table.MASTER_PEMINJAMAN + " (" +
                "kode_buku, " +
                "nim_mahasiswa," +
                "tanggal_kembali," +
                "created_by," +
                "created_time) " +
                "VALUES(?,?,?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getBuku().getId(),
                entity.getMahasiswa().getId(),

                new Timestamp(entity.getTanggalKembali().getTime()),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()));
        return entity;
    }

    @Override
    public Pengembalian update(Pengembalian entity) {
        String sql = "UPDATE " + Table.MASTER_PEMINJAMAN + " SET " +
                "kode_buku = ? , " +
                "nim_mahasiswa = ? , " +
                "tanggal_kembali = ?," +
                "created_by = ? ," +
                "created_time = ? " +
                "WHERE id =  ? ";

        jdbcTemplate.update(sql,
                entity.getBuku().getId(),
                entity.getMahasiswa().getId(),
                new Timestamp(entity.getTanggalKembali().getTime()),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()),
                entity.getId());
        return entity;
    }

    @Override
    public Pengembalian delete(Pengembalian entity) {
        String sql = "DELETE FROM " + Table.MASTER_PEMINJAMAN+ " WHERE id =  ?";

        jdbcTemplate.update(sql, entity.getId());
        return entity;
    }

    @Override
    public Pengembalian findById(long id) {
        String sql = "SELECT * FROM " + Table.MASTER_PEMINJAMAN+ " a " +
                "INNER JOIN " + Table.MASTER_BUKU + " b ON b.id = a.id_buku " +
                "INNER JOIN " + Table.MASTER_MAHASISWA + " c ON c.id = a.id_mahasiswa " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 " +
                "AND a.id = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new PengembalianRowMapper(), id);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return null;
    }

    @Override
    public List<Pengembalian> find(Pengembalian param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.MASTER_PEMINJAMAN + " a " +
                "INNER JOIN " + Table.MASTER_BUKU + " b ON b.id = a.id_buku " +
                "INNER JOIN " + Table.MASTER_MAHASISWA + " c ON c.id = a.id_mahasiswa " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getTanggalKembali() != null) {
            params.add("%" + param.getTanggalKembali() + "%");
            sql += "AND tanggal_kembali = ? ";
        }
        return jdbcTemplate.query(sql, params.toArray(), new PengembalianRowMapper());
    }

    @Override
    public int count(Pengembalian param) {
        String sql = "SELECT COUNT(id) FROM " + Table.MASTER_PEMINJAMAN+ " WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getTanggalKembali() != null) {
            params.add("%" + param.getTanggalKembali() + "%");
            sql += " AND tanggal_kembali = ? ";
        }

        return jdbcTemplate.queryForObject(sql, params.toArray(), Integer.class);
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    class PengembalianRowMapper implements RowMapper<Pengembalian> {
        @Override
        public Pengembalian mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setRealname(rs.getString("realname"));

            Buku buku = new Buku();
            buku.setId(rs.getLong("id"));
            buku.setKode(rs.getString("kode"));
            buku.setJudul(rs.getString("judul"));
            buku.setIsbn(rs.getString("isbn"));
            buku.setNomerRak(rs.getString("nomer_rak"));
            buku.setJumlah_tersedia(rs.getInt("jumlah_tersedia"));
            buku.setJumlah_dipinjam(rs.getInt("jumlah_dipinjam"));
            buku.setJumlah_total(rs.getInt("jumlah_total"));

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setId(rs.getLong("id"));
            mahasiswa.setNIM(rs.getString("nim"));
            mahasiswa.setNamaMahasiswa(rs.getString("nama"));
            mahasiswa.setJurusan(rs.getString("jurusan"));
            mahasiswa.setAlamat(rs.getString("alamat"));

            Peminjaman peminjaman = new Peminjaman();
            peminjaman.setTanggalPinjam(rs.getTimestamp("tanggal_pinjam"));

            Pengembalian pengembalian = new Pengembalian();
            pengembalian.setId(rs.getLong("id"));
            pengembalian.setBuku(buku);
            pengembalian.setMahasiswa(mahasiswa);
            pengembalian.setTanggalKembali(rs.getTimestamp("tanggal_kembali"));
            pengembalian.setCreatedTime(rs.getTimestamp("created_time"));
            pengembalian.setCreatedBy(user);

            return pengembalian;
        }
    }

}
