package com.library.trial.core.dao;

import com.library.trial.core.entity.RoleMenu;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface RoleMenuDAO extends BaseDAO<RoleMenu>{
}
