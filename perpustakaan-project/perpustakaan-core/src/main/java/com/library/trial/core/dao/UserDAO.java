package com.library.trial.core.dao;

import com.library.trial.core.entity.User;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface UserDAO extends BaseDAO<User>{

    User getByUsername(String username);

    String getPassword(User user);

    User getLogUser(String session);

    User getLogUser(Long id);

    User saveLogin(User entity);

    User saveLogout(User entity);

}
