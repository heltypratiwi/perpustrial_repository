package com.library.trial.core.dao;

import com.library.trial.core.entity.SystemParameter;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface SystemParameterDAO extends BaseDAO<SystemParameter>{

    SystemParameter get();
}
