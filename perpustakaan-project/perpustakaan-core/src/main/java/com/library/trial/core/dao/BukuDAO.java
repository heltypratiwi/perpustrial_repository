package com.library.trial.core.dao;

import com.library.trial.core.entity.Buku;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface BukuDAO extends BaseDAO<Buku>{

}
