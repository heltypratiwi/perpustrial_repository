package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.AgamaDAO;
import com.library.trial.core.entity.Agama;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.User;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoggi on 5/12/2017.
 */
@Repository
public class AgamaDAOImpl implements AgamaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Agama save(Agama entity) {
        String sql = "INSERT INTO " + Table.MASTER_AGAMA + " (" +
                "nama," +
                "created_by," +
                "created_time) " +
                "VALUES(?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getNama(),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()));

        return entity;
    }

    @Override
    public Agama update(Agama entity) {
        String sql = "UPDATE " + Table.MASTER_AGAMA + " SET " +
                "nama = ? " +
                "WHERE id =  ? ";

        jdbcTemplate.update(sql,
                entity.getNama(),
                entity.getId());

        return entity;
    }

    @Override
    public Agama delete(Agama entity) {
        String sql = "DELETE FROM " + Table.MASTER_AGAMA + "WHERE id =  ?";

        jdbcTemplate.update(sql, entity.getId());

        return entity;
    }

    @Override
    public Agama findById(long id) {
        String sql = "SELECT * FROM " + Table.MASTER_AGAMA + " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 " +
                "AND a.id = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new AgamaRowMapper(), id);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return null;
    }


    @Override
    public List<Agama> find(Agama param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.MASTER_AGAMA + " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getNama() != null) {
            params.add("%" + param.getNama() + "%");
            sql += "AND nama LIKE ? ";
        }

        return jdbcTemplate.query(sql, params.toArray(), new AgamaRowMapper());
    }

    @Override
    public int count(Agama param) {
        String sql = "SELECT COUNT(id) FROM " + Table.MASTER_AGAMA + "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getNama() != null) {
            params.add("%" + param.getNama() + "%");
            sql += " AND nama LIKE ? ";
        }

        return jdbcTemplate.queryForObject(sql, params.toArray(), Integer.class);
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }


    class AgamaRowMapper implements RowMapper<Agama> {

        @Override
        public Agama mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setRealname(rs.getString("realname"));

            Agama agama = new Agama();
            agama.setId(rs.getLong("id"));
            agama.setNama(rs.getString("nama"));
            agama.setCreatedTime(rs.getTimestamp("created_time"));
            agama.setCreatedBy(user);

            return agama;
        }
    }
}
