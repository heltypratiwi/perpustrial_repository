package com.library.trial.core.aspect;

import com.library.trial.core.common.Result;
import com.library.trial.core.AppCore;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

/**
 * Created by yoggi on 5/5/2017.
 */

@Aspect
@Component
public class AspectService {

    @Around("execution(* com.library.trial.core.service.*.save(..))")
    public Result processSaveTransactional(ProceedingJoinPoint joinPoint) throws Throwable {
        AppCore.getLogger(this).info("***AspectJ*** save is catch !! intercepted :" + joinPoint.getSignature());

        return new Result(validateSaveMethod(joinPoint));
    }
    public String validateSaveMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        if (AppCore.getInstance().getUserFromSession() != null) {
            try {
                joinPoint.proceed();
                return Result.SAVE_SUCCESS;
            } catch (DuplicateKeyException dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.SAVE_DATA_EXIST;
            } catch (DataIntegrityViolationException dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.DB_EXCEPTION;
            } catch (DataAccessException dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.DB_EXCEPTION;
            } catch (Exception dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.SYSTEM_EXCEPTION;
            }
        } else {
            return Result.SESSION_EXPIRED;
        }
    }

    @Around("execution(* com.library.trial.core.service.*.delete(..))")
    public Result processDeleteTransactional(ProceedingJoinPoint joinPoint) throws Throwable {
        AppCore.getLogger(this).info("***AspectJ*** delete is catch !! intercepted : " + joinPoint.getSignature());
        return new Result(validateDeleteMethod(joinPoint));  //some method have no register yet or some method no need register with specific treatment just around here
    }

    /**
     * Catch all transactional for delete from repository method in application
     * and shared all exception if occurs in the method
     *
     * @param joinPoint jointPoint
     * @return Result
     * @throws Throwable
     */
    public String validateDeleteMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        if (AppCore.getInstance().getUserFromSession() != null) {
            try {
                joinPoint.proceed();
                return Result.DELETE_SUCCESS;
            } catch (DataIntegrityViolationException dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.DB_EXCEPTION;
            } catch (DataAccessException dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.DB_EXCEPTION;
            } catch (Exception dae) {
                AppCore.getLogger(this).error(dae.getMessage());
                return Result.SYSTEM_EXCEPTION;
            }
        } else {
            return Result.SESSION_EXPIRED;
        }
    }
}
