package com.library.trial.core.common;

import java.util.Date;

/**
 * Created by yoggi on 5/5/2017.
 */
public class Constant {
    public static final String SESSION_USER = "user";
    public static final String DEFAULT_USER = "USERNAME";
    public static final String DEFAULT_PASSWORD = "PASSWORD";
    public static final String FLAG_DELETE = "[DELETED] ".concat(new Date().getTime() + "");
    public static final Long NULL_ENTITY = null;
    public static final Long DEFAULT_ENTITY = 0l;
}
