package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.MenuDAO;
import com.library.trial.core.entity.*;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
    @Repository
    public class MenuDAOImpl implements MenuDAO {

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public Menu save(Menu entity) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Menu update(Menu entity) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Menu delete(Menu entity) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    @Override
    public Menu findById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
        @Cacheable("menu")
        public List<Menu> find(Menu param, Integer offset, Integer limit) {
            String sql = "SELECT * FROM " + Table.SYSTEM_MENU + " "
                    + "ORDER BY order_number, parent_id ";

            return jdbcTemplate.query(sql, new MenuRowMapper());
        }

        @Override
        public int count(Menu entity) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    @Override
        @Cacheable("menu")
        public List<Menu> find(User user) {
            String sql = "SELECT " +
                    "m.*," +
                    "rm.*," +
                    "rm.id AS id_role_menu " +
                    "FROM " + Table.SYSTEM_MENU + " m " +
                    "INNER JOIN " + Table.SYSTEM_ROLE_MENU + " rm ON rm.menu_id = m.id " +
                    "INNER JOIN " + Table.SYSTEM_ROLE_USER + " ru ON ru.role_id = rm.role_id " +
                    "WHERE ru.user_id = ? " +
                    "AND rm.can_read = 1 " +
                    "GROUP BY m.id " +
                    "ORDER BY order_number, parent_id ";

            return jdbcTemplate.query(sql, new Object[]{user.getId()}, new MenuUserRoleRowMapper());
        }

        @Override
        @Cacheable("menu")
        public List<Menu> find(Role role) {
            String sql = "SELECT " +
                    "m.*," +
                    "rm.*, " +
                    "rm.id AS id_role_menu " +
                    "FROM " + Table.SYSTEM_MENU + " m " +
                    "LEFT JOIN " + Table.SYSTEM_ROLE_MENU + " rm ON rm.menu_id = m.id " +
                    "WHERE rm.role_id = ? " +
                    "ORDER BY order_number, parent_id";

            return jdbcTemplate.query(sql, new Object[]{role.getId()}, new MenuUserRoleRowMapper());
        }

        class MenuRowMapper implements RowMapper<Menu> {

            @Override
            public Menu mapRow(ResultSet rs, int rowNum) throws SQLException {
                Menu menu = new Menu();
                menu.setId(rs.getLong("id"));
                menu.setKode(rs.getString("kode"));
                menu.setIdParent(rs.getLong("parent_id"));
                menu.setNama(rs.getString("nama"));
                menu.setOrderNumber(rs.getInt("order_number"));
                return menu;
            }
        }

        class MenuUserRoleRowMapper implements RowMapper<Menu> {

            @Override
            public Menu mapRow(ResultSet rs, int rowNum) throws SQLException {
                Menu menu = new Menu();
                menu.setId(rs.getLong("id"));
                menu.setKode(rs.getString("kode"));
                menu.setTitle(rs.getString("title"));
                menu.setIdParent(rs.getLong("parent_id"));
                menu.setNama(rs.getString("nama"));
                menu.setOrderNumber(rs.getInt("order_number"));

                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setId(rs.getLong("id_role_menu"));
                roleMenu.setCanRead(rs.getBoolean("can_read"));
                roleMenu.setCanSave(rs.getBoolean("can_save"));
                roleMenu.setCanEdit(rs.getBoolean("can_edit"));
                roleMenu.setCanEdit(rs.getBoolean("can_edit"));
                roleMenu.setCanDelete(rs.getBoolean("can_delete"));
                roleMenu.setCanPrint(rs.getBoolean("can_print"));

                menu.setRoleMenu(roleMenu);
                return menu;
            }
        }
}
