package com.library.trial.core.dao.impl;

import com.library.trial.core.dao.RoleUserDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Role;
import com.library.trial.core.entity.RoleUser;
import com.library.trial.core.entity.User;
import com.library.trial.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
    @Repository
    public class RoleUserDaoImpl implements RoleUserDAO{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public RoleUser save(RoleUser entity) {
        return null;
    }

    @Override
    public List<RoleUser> save(final List<RoleUser> roleUsers) {
        String sql = "INSERT INTO " + Table.SYSTEM_ROLE_USER + "(" +
                "user_id," +
                "role_id) " +
                "VALUES(?,?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                RoleUser roleUser = roleUsers.get(i);
                ps.setLong(1, roleUser.getUser().getId());
                ps.setLong(2, roleUser.getRole().getId());
            }

            @Override
            public int getBatchSize() {
                return roleUsers.size();
            }
        });

        return roleUsers;
    }

    @Override
    public List<RoleUser> find(User user) {
        String sql = "SELECT *FROM " + Table.SYSTEM_ROLE_USER + " ru " +
                "INNER JOIN " + Table.SYSTEM_ROLE + " r ON r.id = ru.role_id " +
                "WHERE ru.user_id = ? ";

        return jdbcTemplate.query(sql, new Object[]{user.getId()}, new RoleUserRowMapper());
    }

    @Override
    public void delete(final List<RoleUser> roleUsers) {
        String sql = "DELETE FROM " + Table.SYSTEM_ROLE_USER + " WHERE user_id = ?";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                RoleUser roleUser = roleUsers.get(i);
                ps.setLong(1, roleUser.getUser().getId());
            }

            @Override
            public int getBatchSize() {
                return roleUsers.size();
            }
        });
    }

    @Override
    public RoleUser update(RoleUser entity) {
        return null;
    }

    @Override
    public RoleUser delete(RoleUser entity) {
        return null;
    }

    @Override
    public RoleUser findById(long id) {
        return null;
    }

    @Override
    public List<RoleUser> find(RoleUser param, Integer offset, Integer limit) {
        return null;
    }

    @Override
    public int count(RoleUser entity) {
        return 0;
    }

    @Override
    public Buku findByKode(Buku buku) {
        return null;
    }

    class RoleUserRowMapper implements RowMapper<RoleUser> {

        @Override
        public RoleUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            Role role = new Role();
            role.setId(rs.getLong("role_id"));

            User user = new User();
            user.setId(rs.getLong("user_id"));

            RoleUser roleUser = new RoleUser();
            roleUser.setId(rs.getLong("id"));
            roleUser.setRole(role);
            roleUser.setUser(user);
            return roleUser;
        }
    }
}
