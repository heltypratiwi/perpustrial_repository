package com.library.trial.core.common;

/**
 * Created by yoggi on 5/9/2017.
 */
public enum Hari {

    SENIN(1),
    SELASA(2),
    RABU(3),
    KAMIS(4),
    JUMAT(5),
    SABTU(6),
    MINGGU(0);

    private int value;

    Hari (int value) {
        this.value = value;
    }
        public int getValue() {
        return value;
    }
}

