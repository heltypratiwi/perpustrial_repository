package com.library.trial.core.dao;

import com.library.trial.core.entity.Menu;
import com.library.trial.core.entity.Role;
import com.library.trial.core.entity.User;

import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
public interface MenuDAO extends BaseDAO<Menu>{

    List<Menu> find(User user);

    List<Menu> find (Role role);
}
