package com.library.trial.core.entity;

import java.io.Serializable;

/**
 * Created by yoggi on 5/10/2017.
 */
public class Mahasiswa extends BaseEntity implements Serializable{
    private String NIM;
    private String namaMahasiswa;
    private String jurusan;
    private String alamat;

    public String getNIM() {
        return NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Mahasiswa mahasiswa = (Mahasiswa) o;

        if (NIM != null ? !NIM.equals(mahasiswa.NIM) : mahasiswa.NIM != null) return false;
        if (namaMahasiswa != null ? !namaMahasiswa.equals(mahasiswa.namaMahasiswa) : mahasiswa.namaMahasiswa != null)
            return false;
        if (jurusan != null ? !jurusan.equals(mahasiswa.jurusan) : mahasiswa.jurusan != null) return false;
        return alamat != null ? alamat.equals(mahasiswa.alamat) : mahasiswa.alamat == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (NIM != null ? NIM.hashCode() : 0);
        result = 31 * result + (namaMahasiswa != null ? namaMahasiswa.hashCode() : 0);
        result = 31 * result + (jurusan != null ? jurusan.hashCode() : 0);
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Mahasiswa{" +
                "NIM='" + NIM + '\'' +
                ", namaMahasiswa='" + namaMahasiswa + '\'' +
                ", jurusan='" + jurusan + '\'' +
                ", alamat='" + alamat + '\'' +
                '}';
    }
}
