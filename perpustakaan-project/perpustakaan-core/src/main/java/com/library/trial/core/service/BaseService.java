package com.library.trial.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Created by yoggi on 5/5/2017.
 */
@Service
public class BaseService {
    @Autowired
    protected TransactionTemplate transactionTemplate;
}
