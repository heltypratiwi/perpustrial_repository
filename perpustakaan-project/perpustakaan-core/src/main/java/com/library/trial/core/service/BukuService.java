package com.library.trial.core.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Constant;
import com.library.trial.core.common.Result;
import com.library.trial.core.dao.BukuDAO;
import com.library.trial.core.entity.Buku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;


/**
 * Created by yoggi on 5/5/2017.
 */
@Service
public class BukuService extends BaseService{

    @Autowired
    private BukuDAO bukuDAO;

    public Result save(final Buku buku) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku.setCreatedBy(AppCore.getInstance().getUserFromSession());
                    if (buku.getId() == null) {
                        bukuDAO.save(buku);
                    } else {
                        bukuDAO.update(buku);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Buku buku) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku.setKode(buku.getKode().concat(Constant.FLAG_DELETE));
                    buku.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    bukuDAO.delete(buku);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Buku findById(long id) {
        return bukuDAO.findById(id);
    }

    public List<Buku> find(Buku param, Integer offset, Integer limit) {
        return bukuDAO.find(param, offset, limit);
    }

    public int count(Buku param) {
        return bukuDAO.count(param);
    }

    public Buku findByKode(Buku buku) {
        return bukuDAO.findByKode(buku);
    }
}
