package com.library.trial.core.entity;

import java.io.Serializable;

/**
 * Created by yoggi on 5/5/2017.
 */
public class Buku extends BaseEntity implements Serializable {

    private String kode;
    private String judul;
    private String isbn;
    private String nomerRak;
    private int jumlah_tersedia;
    private int jumlah_dipinjam;
    private int jumlah_total;


    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getNomerRak() {
        return nomerRak;
    }

    public void setNomerRak(String nomerRak) {
        this.nomerRak = nomerRak;
    }

    public int getJumlah_tersedia() {
        return jumlah_tersedia;
    }

    public void setJumlah_tersedia(int jumlah_tersedia) {
        this.jumlah_tersedia = jumlah_tersedia;
    }

    public int getJumlah_dipinjam() {
        return jumlah_dipinjam;
    }

    public void setJumlah_dipinjam(int jumlah_dipinjam) {
        this.jumlah_dipinjam = jumlah_dipinjam;
    }

    public int getJumlah_total() {
        return jumlah_total;
    }

    public void setJumlah_total(int jumlah_total) {
        this.jumlah_total = jumlah_total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Buku buku = (Buku) o;

        if (jumlah_tersedia != buku.jumlah_tersedia) return false;
        if (jumlah_dipinjam != buku.jumlah_dipinjam) return false;
        if (jumlah_total != buku.jumlah_total) return false;
        if (kode != null ? !kode.equals(buku.kode) : buku.kode != null) return false;
        if (judul != null ? !judul.equals(buku.judul) : buku.judul != null) return false;
        if (isbn != null ? !isbn.equals(buku.isbn) : buku.isbn != null) return false;
        return nomerRak != null ? nomerRak.equals(buku.nomerRak) : buku.nomerRak == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (kode != null ? kode.hashCode() : 0);
        result = 31 * result + (judul != null ? judul.hashCode() : 0);
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + (nomerRak != null ? nomerRak.hashCode() : 0);
        result = 31 * result + jumlah_tersedia;
        result = 31 * result + jumlah_dipinjam;
        result = 31 * result + jumlah_total;
        return result;
    }

    @Override
    public String toString() {
        return "Buku{" +
                "kode='" + kode + '\'' +
                ", judul='" + judul + '\'' +
                ", isbn='" + isbn + '\'' +
                ", nomerRak='" + nomerRak + '\'' +
                ", jumlah_tersedia=" + jumlah_tersedia +
                ", jumlah_dipinjam=" + jumlah_dipinjam +
                ", jumlah_total=" + jumlah_total +
                '}';
    }
}
