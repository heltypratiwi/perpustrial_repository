package com.library.trial.core.service;

import com.library.trial.core.dao.RoleUserDAO;
import com.library.trial.core.entity.RoleUser;
import com.library.trial.core.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yoggi on 5/5/2017.
 */
@Service
public class RoleUserService extends BaseService {
    @Autowired
    private RoleUserDAO roleUserDAO;

    public List<RoleUser> find(User user) {
        return roleUserDAO.find(user);
    }

    public void deleteByUser(List<RoleUser> roleUsers) {
        roleUserDAO.delete(roleUsers);
    }

}
