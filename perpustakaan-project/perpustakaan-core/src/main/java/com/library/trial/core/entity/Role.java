package com.library.trial.core.entity;

import java.io.Serializable;

/**
 * Created by yoggi on 5/5/2017.
 */
public class Role implements Serializable{
    private static final long serialVersionUID = -5683075595824364950L;
    private Long id;
    private String nama;

    public Role() {
    }
    public Role (Long id, String nama){
        this.id = id;
        this.nama = nama;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                '}';
    }
}
