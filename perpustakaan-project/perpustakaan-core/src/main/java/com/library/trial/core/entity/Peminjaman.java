package com.library.trial.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */

public class Peminjaman extends BaseEntity implements Serializable{
    private Buku buku;
    private Mahasiswa mahasiswa;
    private Date tanggalPinjam ;

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Date getTanggalPinjam() {
        return tanggalPinjam;
    }

    public void setTanggalPinjam(Date tanggalPinjam) {
        this.tanggalPinjam = tanggalPinjam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Peminjaman that = (Peminjaman) o;

        if (buku != null ? !buku.equals(that.buku) : that.buku != null) return false;
        if (mahasiswa != null ? !mahasiswa.equals(that.mahasiswa) : that.mahasiswa != null) return false;
        return tanggalPinjam != null ? tanggalPinjam.equals(that.tanggalPinjam) : that.tanggalPinjam == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (buku != null ? buku.hashCode() : 0);
        result = 31 * result + (mahasiswa != null ? mahasiswa.hashCode() : 0);
        result = 31 * result + (tanggalPinjam != null ? tanggalPinjam.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Peminjaman{" +
                "buku=" + buku +
                ", mahasiswa=" + mahasiswa +
                ", tanggalPinjam=" + tanggalPinjam +
                '}';
    }
}
