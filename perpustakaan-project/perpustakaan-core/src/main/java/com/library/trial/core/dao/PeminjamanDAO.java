package com.library.trial.core.dao;

import com.library.trial.core.entity.Peminjaman;

/**
 * Created by yoggi on 5/10/2017.
 */
public interface PeminjamanDAO extends BaseDAO<Peminjaman> {

}
