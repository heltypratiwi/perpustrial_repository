package com.library.trial.core.test.dao;

import com.library.trial.core.AppCore;
import com.library.trial.core.dao.AgamaDAO;
import com.library.trial.core.entity.Agama;
import com.library.trial.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by yoggi on 5/15/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class AgamaDAOTest {
    @Autowired
    private AgamaDAO agamaDAO;
    private Agama agama;

    @Before
    public void init() {
        agama = new Agama();
        agama.setId(29l);
        agama.setNama("Hindu");
        agama.setCreatedBy(new User(1l));
    }

    @Test
    public void delete() {
        AppCore.getLogger(agamaDAO.delete(agama));
    }

    @Test
    public void save() {
        AppCore.getLogger(agamaDAO.save(agama));
    }

}
