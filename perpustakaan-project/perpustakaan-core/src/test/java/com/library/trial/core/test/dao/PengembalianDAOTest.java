package com.library.trial.core.test.dao;

import com.library.trial.core.AppCore;
import com.library.trial.core.dao.PengembalianDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Pengembalian;
import com.library.trial.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by yoggi on 5/15/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class PengembalianDAOTest {
    @Autowired
    private PengembalianDAO pengembalianDAO;
    private Pengembalian pengembalian;
    private Buku buku;
    private Mahasiswa mahasiswa;

    @Before
    public void init() {
        User user = new User();
        user.setRealname("Admin");
        user.setId(1l);

        Buku buku = new Buku();
        buku.setId(1l);
        buku.setJudul("Hujan");

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1l);
        mahasiswa.setNIM("2012001");

        pengembalian = new Pengembalian();
        pengembalian.setId(2l);
        pengembalian.setBuku(buku);
        pengembalian.setMahasiswa(mahasiswa);
        pengembalian.setCreatedBy(user);
        pengembalian.setCreatedTime(new Date());
    }

    @Test
    @Transactional
    public void findById() {
        System.out.println(pengembalianDAO.findById(pengembalian.getId()));
    }

    @Test
    @Transactional
    public void delete() {
        AppCore.getLogger(pengembalianDAO.delete(pengembalian));
    }

    @Test
    @Transactional
    public void save() {
        AppCore.getLogger(pengembalianDAO.save(pengembalian));
    }

    @Test
    @Transactional
    public void update() {
        AppCore.getLogger(pengembalianDAO.update(pengembalian));
    }

    @Test
    @Transactional
    public void count() {
        AppCore.getLogger(pengembalianDAO.count(pengembalian));
    }
}
