package com.library.trial.core.test.service;

import com.library.trial.core.entity.User;
import com.library.trial.core.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yoggi on 5/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
    public class UserServiceTest {

        @Autowired
        private UserService userService;
        private User user;

        @Before
        public void init() {
            user = new User();
            user.setId(2l);
        }

        @Test
        @Transactional
        public void delete() {
            System.out.println(userService.delete(user));
        }
    }
