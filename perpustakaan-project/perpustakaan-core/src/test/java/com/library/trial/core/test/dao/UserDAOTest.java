package com.library.trial.core.test.dao;

import com.library.trial.core.AppCore;
import com.library.trial.core.dao.UserDAO;
import com.library.trial.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yoggi on 5/5/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class UserDAOTest {

        @Autowired
        private UserDAO userDAO;
        private User user;

        @Before
        public void init() {
            user = new User();
            user.setId(2l);
        }

        @Test
        @Transactional
        public void delete() {
            AppCore.getLogger(userDAO.delete(user));
        }
}
