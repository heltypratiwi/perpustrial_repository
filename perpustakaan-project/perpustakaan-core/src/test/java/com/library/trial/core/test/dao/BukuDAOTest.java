package com.library.trial.core.test.dao;

import com.library.trial.core.AppCore;
import com.library.trial.core.dao.BukuDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Role;
import com.library.trial.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by yoggi on 5/5/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class BukuDAOTest {
    @Autowired
    private BukuDAO bukuDAO;
    private Buku buku;

    @Before
    public void init() {
        User user = new User("admin", "admin");
        user.setId(98L);
        user.setDelete(false);
        user.setRole(new Role());
        user.setSession("admin123");

        buku = new Buku();
        buku.setKode("N22");
        buku.setJudul("Hujan");
        buku.setIsbn("978-602-03-2478-4");
        buku.setNomerRak("N2");
        buku.setCreatedBy(user);
    }


    @Test
    public void findById() {
        bukuDAO.findById(buku.getId());
    }

    @Test
    public void delete() {
        AppCore.getLogger(bukuDAO.delete(buku));
    }

    @Test
    public void save() {
        AppCore.getLogger(bukuDAO.save(buku));
    }

    @Test
    public void update() {
        AppCore.getLogger(bukuDAO.update(buku));
    }

    @Test
    public void count() {
        AppCore.getLogger(bukuDAO.count(buku));
    }

    @Test
    public void find() {
        bukuDAO.find(buku, 0, Integer.MAX_VALUE);

    }
}
