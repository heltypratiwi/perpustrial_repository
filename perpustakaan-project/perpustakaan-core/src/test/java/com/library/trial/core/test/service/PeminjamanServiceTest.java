package com.library.trial.core.test.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Constant;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.entity.User;
import com.library.trial.core.service.PeminjamanService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
@WebAppConfiguration
public class PeminjamanServiceTest {
    @Autowired
    private PeminjamanService peminjamanService;
    @Autowired
    private MockHttpSession mockHttpSession;
    private Peminjaman peminjaman;
    private Peminjaman param;

    @Before
    public void init() {

        mockHttpSession.setAttribute(Constant.SESSION_USER, new User(1l));
        peminjaman = new Peminjaman();
        Buku buku = new Buku();
        buku.setId(1l);
        buku.setJudul("Hujan");

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1l);
        mahasiswa.setNIM("2012001");

        peminjaman = new Peminjaman();
        peminjaman.setId(2l);
        peminjaman.setBuku(buku);
        peminjaman.setMahasiswa(mahasiswa);
        peminjaman.setCreatedBy(new User(1l));
        peminjaman.setCreatedTime(new Date());

        param = new Peminjaman();

    }

    @Test
    @Transactional
    public void delete() {
        AppCore.getLogger(peminjamanService.delete(peminjaman));
    }

    @Test
    @Transactional
    public void save() {
        AppCore.getLogger(peminjamanService.save(peminjaman));
    }

    @Test
    @Transactional
    public void count() {
        AppCore.getLogger(peminjamanService.count(peminjaman));
    }

    @Test
    @Transactional
    public void find() {
        AppCore.getLogger(peminjamanService.find(peminjaman, 0, Integer.MAX_VALUE));
    }

    @Test
    @Transactional
    public void findById() {
        AppCore.getLogger(peminjamanService.findById(peminjaman.getId()));
    }
}
