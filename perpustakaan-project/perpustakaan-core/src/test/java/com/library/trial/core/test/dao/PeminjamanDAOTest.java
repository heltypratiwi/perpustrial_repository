package com.library.trial.core.test.dao;

import com.library.trial.core.dao.PeminjamanDAO;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.Mahasiswa;
import com.library.trial.core.entity.Peminjaman;
import com.library.trial.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by yoggi on 5/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class PeminjamanDAOTest {
    @Autowired
    private PeminjamanDAO peminjamanDAO;
    private Peminjaman peminjaman;
    private Buku buku;
    private Mahasiswa mahasiswa;

    @Before
    public void init() {
        User user = new User();
        user.setRealname("Admin");
        user.setId(1l);

        Buku buku = new Buku();
        buku.setId(1l);
        buku.setJudul("Hujan");

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1l);
        mahasiswa.setNIM("2012001");

        peminjaman = new Peminjaman();
        peminjaman.setId(2l);
        peminjaman.setBuku(buku);
        peminjaman.setMahasiswa(mahasiswa);
        peminjaman.setCreatedBy(user);
        peminjaman.setCreatedTime(new Date());

    }

    @Test
    public void save() {
        peminjamanDAO.save(peminjaman);
    }

    @Test
    public void delete() {
        peminjamanDAO.delete(peminjaman);
    }


    @Test
    public void update() {
        peminjamanDAO.update(peminjaman);
    }


    @Test
    public void find() {
        peminjamanDAO.find(peminjaman, 0, Integer.MAX_VALUE);
    }


    @Test
    public void findById() {
        peminjamanDAO.findById(peminjaman.getId());
    }


    @Test
    public void count() {
        peminjamanDAO.count(peminjaman);
    }
}
