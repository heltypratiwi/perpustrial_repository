package com.library.trial.core.test.service;

import com.library.trial.core.AppCore;
import com.library.trial.core.common.Constant;
import com.library.trial.core.entity.Buku;
import com.library.trial.core.entity.User;
import com.library.trial.core.service.BukuService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yoggi on 5/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
@WebAppConfiguration
public class BukuServiceTest {
    @Autowired
    private MockHttpSession mockHttpSession;
    @Autowired
    private BukuService bukuService;
    private Buku buku;


    @Before
    public void init() {
        mockHttpSession.setAttribute(Constant.SESSION_USER, new User(1l));

        buku = new Buku();
        buku.setKode("N22");
        buku.setJudul("Hujan");
        buku.setIsbn("978-602-03-2478-4");
        buku.setNomerRak("N2");
        buku.setCreatedBy((User) mockHttpSession.getAttribute(Constant.SESSION_USER));

    }


    @Test
    @Transactional
    public void delete() {
        System.out.println(bukuService.delete(buku));
    }

    @Test
    @Transactional
    public void findById() {
        System.out.println(bukuService.findById(buku.getId()));
    }

    @Test
    @Transactional
    public void find() {
        bukuService.find(buku, 0, Integer.MAX_VALUE);
    }

    @Test
    @Transactional
    public void save() {
        AppCore.getLogger(bukuService.save(buku));
    }

    @Test
    @Transactional
    public void count() {
        AppCore.getLogger(bukuService.count(buku));

    }
}
